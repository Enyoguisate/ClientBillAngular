USE [master]
GO
/****** Object:  Database [testClientBill]    Script Date: 11-Aug-17 11:26:26 AM ******/
CREATE DATABASE [testClientBill]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'testClientBill', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\testClientBill.mdf' , SIZE = 3072KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'testClientBill_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\testClientBill_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [testClientBill] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [testClientBill].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [testClientBill] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [testClientBill] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [testClientBill] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [testClientBill] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [testClientBill] SET ARITHABORT OFF 
GO
ALTER DATABASE [testClientBill] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [testClientBill] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [testClientBill] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [testClientBill] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [testClientBill] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [testClientBill] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [testClientBill] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [testClientBill] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [testClientBill] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [testClientBill] SET  DISABLE_BROKER 
GO
ALTER DATABASE [testClientBill] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [testClientBill] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [testClientBill] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [testClientBill] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [testClientBill] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [testClientBill] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [testClientBill] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [testClientBill] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [testClientBill] SET  MULTI_USER 
GO
ALTER DATABASE [testClientBill] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [testClientBill] SET DB_CHAINING OFF 
GO
ALTER DATABASE [testClientBill] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [testClientBill] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [testClientBill] SET DELAYED_DURABILITY = DISABLED 
GO
USE [testClientBill]
GO
/****** Object:  User [victor]    Script Date: 11-Aug-17 11:26:27 AM ******/
CREATE USER [victor] FOR LOGIN [victor] WITH DEFAULT_SCHEMA=[dbo]
GO
ALTER ROLE [db_owner] ADD MEMBER [victor]
GO
ALTER ROLE [db_accessadmin] ADD MEMBER [victor]
GO
ALTER ROLE [db_securityadmin] ADD MEMBER [victor]
GO
ALTER ROLE [db_ddladmin] ADD MEMBER [victor]
GO
ALTER ROLE [db_backupoperator] ADD MEMBER [victor]
GO
ALTER ROLE [db_datareader] ADD MEMBER [victor]
GO
ALTER ROLE [db_datawriter] ADD MEMBER [victor]
GO
ALTER ROLE [db_denydatareader] ADD MEMBER [victor]
GO
ALTER ROLE [db_denydatawriter] ADD MEMBER [victor]
GO
/****** Object:  Table [dbo].[Bills]    Script Date: 11-Aug-17 11:26:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Bills](
	[idBill] [int] IDENTITY(1,1) NOT NULL,
	[billDate] [datetime] NULL,
	[billTotal] [numeric](18, 2) NULL,
	[idClient] [int] NULL,
	[billNumber] [int] NULL,
 CONSTRAINT [PK_Bills] PRIMARY KEY CLUSTERED 
(
	[idBill] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Clients]    Script Date: 11-Aug-17 11:26:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Clients](
	[idClient] [int] IDENTITY(1,1) NOT NULL,
	[clientFirstName] [varchar](50) NULL,
	[clientLastName] [varchar](50) NULL,
	[clientEmail] [varchar](50) NULL,
	[clientPhone] [varchar](50) NULL,
	[clientAddress] [varchar](50) NULL,
	[clientCreated] [datetime] NULL,
	[clientLastAccess] [datetime] NULL,
 CONSTRAINT [PK_Clients] PRIMARY KEY CLUSTERED 
(
	[idClient] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Detail]    Script Date: 11-Aug-17 11:26:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Detail](
	[detailNumber] [int] IDENTITY(1,1) NOT NULL,
	[billNumber] [int] NULL,
	[idProduct] [int] NULL,
	[quantity] [int] NULL,
	[TotalDetail] [numeric](18, 2) NULL,
 CONSTRAINT [PK_Detail] PRIMARY KEY CLUSTERED 
(
	[detailNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Products]    Script Date: 11-Aug-17 11:26:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Products](
	[idProduct] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](100) NULL,
	[price] [numeric](18, 2) NULL,
	[stock] [int] NULL,
 CONSTRAINT [PK_Products] PRIMARY KEY CLUSTERED 
(
	[idProduct] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Users]    Script Date: 11-Aug-17 11:26:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Users](
	[idUser] [int] IDENTITY(1,1) NOT NULL,
	[userUsername] [varchar](50) NULL,
	[userPassword] [varchar](50) NULL,
 CONSTRAINT [PK_Users] PRIMARY KEY CLUSTERED 
(
	[idUser] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Index [FK_Bills]    Script Date: 11-Aug-17 11:26:27 AM ******/
CREATE NONCLUSTERED INDEX [FK_Bills] ON [dbo].[Bills]
(
	[idBill] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  StoredProcedure [dbo].[SP_DELBILLBYID]    Script Date: 11-Aug-17 11:26:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SP_DELBILLBYID]
	-- Add the parameters for the stored procedure here
	@idBill int	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- Insert statements for procedure here
	SET NOCOUNT ON;
	DELETE FROM Bills
	WHERE
	(@idBill = idBill)
END


GO
/****** Object:  StoredProcedure [dbo].[SP_DELCLIENTBYID]    Script Date: 11-Aug-17 11:26:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SP_DELCLIENTBYID]
	-- Add the parameters for the stored procedure here
	@idClient int	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DELETE FROM Clients
	WHERE
	(@idClient = idClient)
END

GO
/****** Object:  StoredProcedure [dbo].[SP_DELDETAILSBYBILLNUMBER]    Script Date: 11-Aug-17 11:26:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SP_DELDETAILSBYBILLNUMBER]
	-- Add the parameters for the stored procedure here
	@billNumber int	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- Insert statements for procedure here
	SET NOCOUNT ON;
	DELETE FROM Detail
	WHERE
	(@billNumber = billNumber)
END


GO
/****** Object:  StoredProcedure [dbo].[SP_GETBILLSBYCLIENTID]    Script Date: 11-Aug-17 11:26:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SP_GETBILLSBYCLIENTID]
	-- Add the parameters for the stored procedure here
	@idClient int
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT b.idBill, b.billDate, b.billTotal,b.idClient,b.billNumber,
	c.clientFirstName +' '+c.clientLastName as clientName
	FROM Bills b
	inner join Clients c on(c.idClient=b.idClient)
	WHERE
	(@idClient is null or @idClient = b.idClient)
END


GO
/****** Object:  StoredProcedure [dbo].[SP_GETBILLSBYID]    Script Date: 11-Aug-17 11:26:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SP_GETBILLSBYID]
	-- Add the parameters for the stored procedure here
	@idBill int = null
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT b.*, c.clientFirstName +' '+c.clientLastName as clientName FROM Bills b
	inner join Clients c on(c.idClient = b.idClient)
	WHERE
	(@idBill is null or @idBill=idBill)
END


GO
/****** Object:  StoredProcedure [dbo].[SP_GETCLIENTBILLBYID]    Script Date: 11-Aug-17 11:26:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SP_GETCLIENTBILLBYID] 
	-- Add the parameters for the stored procedure here
	@idClient int,
	@idBill int	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	select * from clients
	inner join bills
	on (clients.idClient = @idClient and bills.idClient = @idClient and bills.idBill = @idBill)
	inner join detail
	on (detail.billNumber = bills.billNumber)
	inner join products
	on(Products.idProduct = Detail.idProduct)
END

GO
/****** Object:  StoredProcedure [dbo].[SP_GETCLIENTSBILLS]    Script Date: 11-Aug-17 11:26:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SP_GETCLIENTSBILLS]
	-- Add the parameters for the stored procedure here
	@idClient int = null,
	@idBill int = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * from clients 
	inner join bills on (bills.idClient=Clients.idClient)
	inner join detail on(bills.billNumber=detail.billNumber)
	inner join products on(detail.idProduct=Products.idProduct)
END

GO
/****** Object:  StoredProcedure [dbo].[SP_GETCLIENTSBYID]    Script Date: 11-Aug-17 11:26:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SP_GETCLIENTSBYID]
	-- Add the parameters for the stored procedure here
	@idClient int = null
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * FROM Clients
	WHERE
	(@idClient is null or @idClient=idClient)
END

GO
/****** Object:  StoredProcedure [dbo].[SP_GETDETAILSBYBILLNUMBER]    Script Date: 11-Aug-17 11:26:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SP_GETDETAILSBYBILLNUMBER]
	-- Add the parameters for the stored procedure here
	@billNumber int = null
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT d.*, p.name as name FROM Detail d
	inner join Products p on(p.idProduct = d.idProduct)
	WHERE
	(@billNumber is null or @billNumber=d.billNumber)
END



GO
/****** Object:  StoredProcedure [dbo].[SP_GETPRODUCTSBYID]    Script Date: 11-Aug-17 11:26:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SP_GETPRODUCTSBYID]
	-- Add the parameters for the stored procedure here
	@idProduct int = null
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * FROM Products
	WHERE(@idProduct is null or @idProduct = idProduct)
END





GO
/****** Object:  StoredProcedure [dbo].[SP_INSBILL]    Script Date: 11-Aug-17 11:26:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SP_INSBILL]
	-- Add the parameters for the stored procedure here
	@billDate datetime = null,
	@billTotal numeric(18,2) = null,
	@billNumber int = null,
	@idClient int = null	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT into Bills(billDate,billTotal,billNumber,idClient)
	values(@billDate,@billTotal,@billNumber,@idClient)
END

GO
/****** Object:  StoredProcedure [dbo].[SP_INSCLIENTS]    Script Date: 11-Aug-17 11:26:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SP_INSCLIENTS]
	-- Add the parameters for the stored procedure here
	@clientFirstName varchar(50) = null,
	@clientLastName varchar(50) = null,
	@clientEmail varchar(50) = null,
	@clientPhone varchar(50) = null,
	@clientAddress varchar(50) = null,
	@clientCreated datetime = null,
	@clientLastAccess datetime = null
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT into Clients(clientFirstName,clientLastName,clientEmail,clientPhone,clientAddress,clientCreated,clientLastAccess)
	values(@clientFirstName,@clientLastName,@clientEmail,@clientPhone,@clientAddress,@clientCreated,@clientLastAccess)
END

GO
/****** Object:  StoredProcedure [dbo].[SP_INSUSER]    Script Date: 11-Aug-17 11:26:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Victor Martin
-- Create date: 02/08/2017
-- Description:	This SP is use to insert a User
-- =============================================
CREATE PROCEDURE [dbo].[SP_INSUSER]
	-- Add the parameters for the stored procedure here
	@userUsername varchar(50) = null,
	@userPassword varchar(50) = null
	
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT into USERS(userUsername,userPassword)
	values(@userUserName,@userPassword)
END

GO
/****** Object:  StoredProcedure [dbo].[SP_UPDBILL]    Script Date: 11-Aug-17 11:26:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SP_UPDBILL]
	-- Add the parameters for the stored procedure here
	@idBill int,
	@billDate datetime = null,
	@billTotal numeric(18,2) = null,
	@billNumber int = null,
	@idClient int = null
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE Bills
	set
	billDate = ISNULL(@billDate, billDate),
	billTotal = ISNULL(@billTotal, billTotal),
	billNumber = ISNULL(@billNumber, billNumber),
	idClient = ISNULL(@idClient, idClient)
	WHERE
	(@idBill is null or @idBill = idBill)
END



GO
/****** Object:  StoredProcedure [dbo].[SP_UPDCLIENT]    Script Date: 11-Aug-17 11:26:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SP_UPDCLIENT]
	-- Add the parameters for the stored procedure here
	@idClient int,
	@clientFirstName varchar(50) = null,
	@clientLastName varchar(50) = null,
	@clientEmail varchar(50) = null,
	@clientPhone varchar(50) = null,
	@clientAddress varchar(50) = null	
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	UPDATE Clients
	set
		clientFirstName = ISNULL(@clientFirstName,clientFirstName),
		clientLastName = ISNULL(@clientLastName,clientLastName),
		clientEmail = ISNULL(@clientEmail,clientEmail),
		clientPhone = ISNULL(@clientPhone,clientPhone),
		clientAddress = ISNULL(@clientAddress,clientAddress)		
		FROM Clients
		where
		(idClient=@idClient)		
END

GO
/****** Object:  StoredProcedure [dbo].[SP_VALUSEREXISTENCE]    Script Date: 11-Aug-17 11:26:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SP_VALUSEREXISTENCE]
	-- Add the parameters for the stored procedure here
	@userUsername varchar (50)= null,
	@userPassword varchar(50) = null	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * from Users
	WHERE
	(@userUsername = userUsername)
	and
	(@userPassword = userPassword)	
END

GO
USE [master]
GO
ALTER DATABASE [testClientBill] SET  READ_WRITE 
GO
