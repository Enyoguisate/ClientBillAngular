
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Victor Martin
-- Create date: 04/08/2017
-- Description:	This SP is use to get clients by id
-- =============================================
go
IF NOT  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[dbo].[SP_GETCLIENTSBILLS]') 
AND type in (N'P', N'PC')) 
EXEC( 'CREATE PROCEDURE [dbo].[SP_GETCLIENTSBILLS] as return -1')
go
alter PROCEDURE SP_GETCLIENTSBILLS
	-- Add the parameters for the stored procedure here
	@idClient int = null,
	@idBill int = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * from clients 
	inner join bills on (bills.idClient=Clients.idClient)
	inner join detail on(bills.billNumber=detail.billNumber)
	inner join products on(detail.idProduct=Products.idProduct)
END
GO
