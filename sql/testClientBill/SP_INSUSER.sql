USE [testClientBill]
GO
/****** Object:  StoredProcedure [dbo].[SP_INSUSER]    
Script Date: 02-Aug-17 9:58:01 AM ******/
go
IF NOT  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[dbo].[SP_INSUSER]') 
AND type in (N'P', N'PC')) 
EXEC( 'CREATE PROCEDURE [dbo].[SP_INSUSER] as return -1')
go
-- =============================================
-- Author:		Victor Martin
-- Create date: 02/08/2017
-- Description:	This SP is use to insert a User
-- =============================================
alter PROCEDURE SP_INSUSER
	-- Add the parameters for the stored procedure here
	@userUsername varchar(50) = null,
	@userPassword varchar(50) = null
	
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT into USERS(userUsername,userPassword)
	values(@userUserName,@userPassword)
END
GO
