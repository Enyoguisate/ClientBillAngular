USE [testClientBill]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

alter PROCEDURE [dbo].[SP_FIND_STRING_IN_TABLE] @stringToFind VARCHAR(100), @schema varchar(10), @table varchar(50), @colarray VARCHAR(MAX)
AS

BEGIN TRY
	
	
   DECLARE @sqlCommand varchar(max) = 'SELECT * FROM [' + @schema + '].[' + @table + '] WHERE ' 
	   
   SELECT @sqlCommand = @sqlCommand + '[' + COLUMN_NAME + '] LIKE ''' + @stringToFind + '%'' OR '
   FROM INFORMATION_SCHEMA.COLUMNS 
   WHERE TABLE_SCHEMA = @schema
   AND TABLE_NAME = @table 
   --AND DATA_TYPE IN ('char','nchar','ntext','nvarchar','text','varchar')
   AND COLUMN_NAME IN (SELECT COLUMN_NAME FROM [dbo].[Split](@colarray, '|') WHERE COLUMN_NAME like '%'+Data+'%')

   SET @sqlCommand = left(@sqlCommand,len(@sqlCommand)-3)
   EXEC (@sqlCommand)
   --PRINT @sqlCommand
END TRY

BEGIN CATCH 
   PRINT 'There was an error. Check to make sure object exists.'
   PRINT error_message()
END CATCH 