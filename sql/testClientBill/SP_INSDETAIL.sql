USE [testClientBill]
GO
-- =============================================
-- Author:		Victor Martin
-- Create date: 19/08/17
-- Description:	This SP is used to insert a detail
-- =============================================
go
IF NOT  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[dbo].[SP_INSDETAIL]') 
AND type in (N'P', N'PC')) 
EXEC( 'CREATE PROCEDURE [dbo].[SP_INSDETAIL] as return -1')
go
alter PROCEDURE SP_INSDETAIL
	-- Add the parameters for the stored procedure here
	@billNumber int = null,
	@idProduct  int = null,
    @quantity  int = null,
    @TotalDetail  int = null
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT into Detail(billNumber,idProduct,quantity,TotalDetail)
	values(@billNumber, @idProduct, @quantity, @TotalDetail)
END

