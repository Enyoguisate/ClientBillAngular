USE [testClientBill]
GO
/****** Object:  StoredProcedure [dbo].[SP_DELDETAILSBYBILLNUMBER]    Script Date: 08-Aug-17 19:55:01 pM ******/

-- =============================================
-- Author:		Victor Martin
-- Create date: 08/08/2017
-- Description:	This SP is use to delete a detail by bill number
-- =============================================
go
IF NOT  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[dbo].[SP_DELDETAILSBYBILLNUMBER]') 
AND type in (N'P', N'PC')) 
EXEC( 'CREATE PROCEDURE [dbo].[SP_DELDETAILSBYBILLNUMBER] as return -1')
go
alter PROCEDURE SP_DELDETAILSBYBILLNUMBER
	-- Add the parameters for the stored procedure here
	@billNumber int	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- Insert statements for procedure here
	SET NOCOUNT ON;
	DELETE FROM Detail
	WHERE
	(@billNumber = billNumber)
END

