USE [testClientBill]
GO
/****** Object:  StoredProcedure [dbo].[SP_GET_USERAVAIBILITY]    Script Date: 02-Aug-17 9:58:01 AM ******/

-- =============================================
-- Author:		Victor Martin
-- Create date: 02/08/2017
-- Description:	This SP is use to get clients by id
-- =============================================
go
IF NOT  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[dbo].[SP_GETCLIENTSBYID]') 
AND type in (N'P', N'PC')) 
EXEC( 'CREATE PROCEDURE [dbo].[SP_GETCLIENTSBYID] as return -1')
go
alter PROCEDURE SP_GETCLIENTSBYID
	-- Add the parameters for the stored procedure here
	@idClient int = null
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT idClient FROM Clients
	WHERE
	(@idClient is null or @idClient=idClient)
END
