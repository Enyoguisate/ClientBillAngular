USE [testClientBill]
GO
create function dbo.SearchClientBy(@searchBy varchar(max),@searchValues varchar(max))
returns @tableResult table(idClient int,
						clientFirstName varchar(50),
						clientLastName varchar(50),
						clientEmail varchar(50),
						clientPhone varchar(50),
						clientAddress varchar(50),
						clientCreated dateTime,
						clientLastAccess datetime)
as
begin
	declare @temp table(idClient int,
						clientFirstName varchar(50),
						clientLastName varchar(50),
						clientEmail varchar(50),
						clientPhone varchar(50),
						clientAddress varchar(50),
						clientCreated dateTime,
						clientLastAccess datetime)
	Declare @lastField int = 1
	while len(@searchBy) > 0 
		begin				
			declare @pipePositionA int = charindex('|', @searchBy)  --get pos of pipe
			if @pipePositionA = 0 
				set @pipePositionA = len(@searchBy) + 1
			declare @pipePositionB int = charindex('|', @searchValues) --get pos of pipe
			if @pipePositionB = 0 
				set @pipePositionB = len(@searchValues)+1
			declare @fieldA varchar(1000) = substring(@searchBy, 1, @pipePositionA - 1) --get field
			set @searchBy = substring(@searchBy, @pipePositionA + 1, len(@searchBy)) --update string		
			declare @fieldB varchar(1000) = substring(@searchValues, 1, @pipePositionB - 1) --get field
			set @searchValues = substring(@searchValues, @pipePositionB + 1, len(@searchValues)) --update string	
			
			insert into @temp 
			select c.idClient,c.clientFirstName,c.clientLastName,c.clientEmail,c.clientPhone,
			c.clientAddress,c.clientCreated,c.clientLastAccess
			from Clients c 
			where c.clientFirstName = @fieldB 
			or c.clientLastName = @fieldB 
			or c.clientEmail = @fieldB 
			or c.clientPhone = @fieldB 
			or c.clientAddress = @fieldB
		end
		insert into @tableResult 
		select distinct t.idClient,t.clientFirstName,t.clientLastName,t.clientEmail,t.clientPhone,
		t.clientAddress,t.clientCreated,t.clientLastAccess 
		from @temp t 
	return		
end
	
