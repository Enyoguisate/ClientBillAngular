-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Victor Martin
-- Create date: 03/08/2017
-- Description:	This SP is used to get a client bill with detail
-- =============================================
go
IF NOT  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[dbo].[SP_GETCLIENTBILLBYID]') 
AND type in (N'P', N'PC')) 
EXEC( 'CREATE PROCEDURE [dbo].[SP_GETCLIENTBILLBYID] as return -1')
go
ALTER PROCEDURE SP_GETCLIENTBILLBYID 
	-- Add the parameters for the stored procedure here
	@idClient int,
	@idBill int	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	select * from clients
	inner join bills
	on (clients.idClient = @idClient and bills.idClient = @idClient and bills.idBill = @idBill)
	inner join detail
	on (detail.billNumber = bills.billNumber)
	inner join products
	on(Products.idProduct = Detail.idProduct)
END
GO
