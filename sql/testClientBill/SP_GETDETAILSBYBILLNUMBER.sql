USE [testClientBill]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Victor Martin
-- Create date: 07/08/17
-- Description:	This sp is use to get details by bill number
-- =============================================
go
IF NOT  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[dbo].[SP_GETDETAILSBYBILLNUMBER]') 
AND type in (N'P', N'PC')) 
EXEC( 'CREATE PROCEDURE [dbo].[SP_GETDETAILSBYBILLNUMBER] as return -1')
go
ALTER PROCEDURE [dbo].[SP_GETDETAILSBYBILLNUMBER]
	-- Add the parameters for the stored procedure here
	@billNumber int = null
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT d.*, p.name as name FROM Detail d
	inner join Products p on(p.idProduct = d.idProduct)
	WHERE
	(@billNumber is null or @billNumber=d.billNumber)
END


