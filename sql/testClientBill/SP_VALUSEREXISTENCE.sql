USE [testClientBill]
GO
/****** Object:  StoredProcedure [dbo].[SP_VALUSEREXISTENCE]    
Script Date: 02-Aug-17 9:58:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Victor Martin
-- Create date: 02/08/2017
-- Description:	This SP is use to validate login
-- =============================================
go
IF NOT  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[dbo].[SP_VALUSEREXISTENCE]') 
AND type in (N'P', N'PC')) 
EXEC( 'CREATE PROCEDURE [dbo].[SP_VALUSEREXISTENCE] as return -1')
go
alter PROCEDURE SP_VALUSEREXISTENCE
	-- Add the parameters for the stored procedure here
	@userUsername varchar (50)= null,
	@userPassword varchar(50) = null	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * from Users
	WHERE
	(@userUsername = userUsername)
	and
	(@userPassword = userPassword)	
END
