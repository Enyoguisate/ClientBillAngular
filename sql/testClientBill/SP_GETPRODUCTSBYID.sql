USE [testClientBill]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Victor Martin
-- Create date: 07/08/17
-- Description:	This sp is use to get products by product id
-- =============================================
go
IF NOT  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[dbo].[SP_GETPRODUCTSBYID]') 
AND type in (N'P', N'PC')) 
EXEC( 'CREATE PROCEDURE [dbo].[SP_GETPRODUCTSBYID] as return -1')
go
ALTER PROCEDURE [dbo].[SP_GETPRODUCTSBYID]
	-- Add the parameters for the stored procedure here
	@idProduct int = null
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * FROM Products
	WHERE(@idProduct is null or @idProduct = idProduct)
END




