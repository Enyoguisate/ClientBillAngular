USE [testClientBill]
GO
/****** Object:  StoredProcedure [dbo].[SP_DELBILLBYID]    Script Date: 02-Aug-17 11:41:01 AM ******/

-- =============================================
-- Author:		Victor Martin
-- Create date: 02/08/2017
-- Description:	This SP is use to delete a bill
-- =============================================
go
IF NOT  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[dbo].[SP_DELBILLBYID]') 
AND type in (N'P', N'PC')) 
EXEC( 'CREATE PROCEDURE [dbo].[SP_DELBILLBYID] as return -1')
go
alter PROCEDURE SP_DELBILLBYID
	-- Add the parameters for the stored procedure here
	@idBill int	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- Insert statements for procedure here
	SET NOCOUNT ON;
	DELETE FROM Bills
	WHERE
	(@idBill = idBill)
END

