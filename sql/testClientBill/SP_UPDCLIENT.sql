USE [testClientBill]
GO
/****** Object:  StoredProcedure [dbo].[SP_UPDCLIENT]    Script Date: 05-Aug-17 8:25:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[SP_UPDCLIENT]
	-- Add the parameters for the stored procedure here
	@idClient int,
	@clientFirstName varchar(50) = null,
	@clientLastName varchar(50) = null,
	@clientEmail varchar(50) = null,
	@clientPhone varchar(50) = null,
	@clientAddress varchar(50) = null	
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	UPDATE Clients
	set
		clientFirstName = ISNULL(@clientFirstName,clientFirstName),
		clientLastName = ISNULL(@clientLastName,clientLastName),
		clientEmail = ISNULL(@clientEmail,clientEmail),
		clientPhone = ISNULL(@clientPhone,clientPhone),
		clientAddress = ISNULL(@clientAddress,clientAddress)		
		FROM Clients
		where
		(idClient=@idClient)		
END
