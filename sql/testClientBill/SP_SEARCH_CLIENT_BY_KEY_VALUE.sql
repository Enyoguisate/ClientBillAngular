-- =============================================
-- Author:		Victor Martin
-- Create date: 11/09/2017
-- Description:	This SP is used to search a client by key and value
-- =============================================
go
IF NOT  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[dbo].[SP_SEARCH_CLIENT_BY_KEY_VALUE]') 
AND type in (N'P', N'PC')) 
EXEC( 'CREATE PROCEDURE [dbo].[SP_SEARCH_CLIENT_BY_KEY_VALUE] as return -1')

go
alter PROCEDURE SP_SEARCH_CLIENT_BY_KEY_VALUE
	@searchFields varchar(max),
	@serchValues varchar(max) 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	select * from dbo.SearchClientBy(@searchFields,@serchValues)	
END
GO
