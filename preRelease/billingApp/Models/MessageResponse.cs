﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace billingApp.Models
{
    public class MessageResponse
    {
        #region Fields

        private HttpStatusCode statusCode;
        
        [JsonIgnore]
        public HttpStatusCode StatusCode { get { return this.statusCode; } }

        #endregion
        #region constructors

        public MessageResponse(HttpStatusCode statusCode)
        {
            this.statusCode = statusCode;
        }

        #endregion

        #region Methods
        public HttpResponseMessage GetHttpResponseMessage()
        {
            return GetHttpResponseMessage(null);
        }

        public HttpResponseMessage GetHttpResponseMessage(Dictionary<string, string> headers)
        {

            var formater = GlobalConfiguration.Configuration.Formatters.JsonFormatter;
            formater.SerializerSettings.ContractResolver = new DefaultContractResolver { IgnoreSerializableAttribute = true };

            HttpResponseMessage message = new HttpResponseMessage(this.statusCode);
            message.Content = new ObjectContent(typeof(MessageResponse), this, formater);

            if (headers != null)
            {
                foreach (KeyValuePair<string, string> item in headers)
                    message.Headers.Add(item.Key, item.Value);
            }

            return message;
        }


        #endregion
    }
}