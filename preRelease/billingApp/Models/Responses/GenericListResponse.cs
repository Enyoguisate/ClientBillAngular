﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;


namespace billingApp.Models.Responses
{
    public class GenericListResponse<T> : MessageResponse
    {
        #region Fields

        public HttpStatusCode Code
        {
            get { return base.StatusCode; }
        }

        public List<T> list { get; set; } 

        #endregion

        public GenericListResponse(HttpStatusCode statusCode, List<T> list) : base(statusCode)
        {
            this.list = list;
        } 

    }
}