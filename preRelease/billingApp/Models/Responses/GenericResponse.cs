﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;

namespace billingApp.Models.Responses
{
    public class GenericResponse<T> : MessageResponse
    {
        #region Fields

        public HttpStatusCode Code
        {
            get { return base.StatusCode; }
        }

        public T element { get; set; }


        #endregion

        public GenericResponse(HttpStatusCode statusCode, T element) : base(statusCode)
        {
            this.element = element;
        }


    }
}