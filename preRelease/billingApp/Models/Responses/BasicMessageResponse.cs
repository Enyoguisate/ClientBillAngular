﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;

namespace billingApp.Models.Responses
{
    public class BasicMessageResponse : MessageResponse
    {
        #region Fields

        public HttpStatusCode Code
        {
            get { return base.StatusCode; }
        }

        public string Message { get; set; }

        #endregion

        public BasicMessageResponse(HttpStatusCode statusCode, string message) : base(statusCode)
        {
            this.Message = message;
        }

    }
}