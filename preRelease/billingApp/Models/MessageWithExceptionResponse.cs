﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace billingApp.Models
{
    public class MessageWithExceptionResponse
    {
        public MessageResponse Response { get; set; }
        public bool ThrowException { get; set; }
        public new Dictionary<string, string> Headers { get; set; }
    }
}