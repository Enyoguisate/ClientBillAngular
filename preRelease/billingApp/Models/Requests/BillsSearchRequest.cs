﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace billingApp.Models.Requests
{
    public class BillsSearchRequest
    {
        public List<KeyValuePair<string, string>> SearchFields { get; set; }

        public BillsSearchRequest()
        {
            SearchFields = new List<KeyValuePair<string, string>>();
        }
    }
}