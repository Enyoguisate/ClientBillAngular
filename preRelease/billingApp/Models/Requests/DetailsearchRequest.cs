﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace billingApp.Models.Requests
{
    public class DetailsSearchRequest
    {
        public List<KeyValuePair<string, string>> SearchFields { get; set; }

        public DetailsSearchRequest()
        {
            SearchFields = new List<KeyValuePair<string, string>>();
        }
    }
}