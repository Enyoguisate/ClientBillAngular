﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using billingApp.Models;
using billingApp.Models.Requests;
using billingApp.Models.Responses;
using billingAppModels;
using billingAppService.Interfaces;

namespace billingApp.Controllers
{
    [RoutePrefix("api/Products")]
    public class ProductsController : BaseController
    {
        private IProductsService _productsService;
        
        public ProductsController(IProductsService productsService)
        {
            _productsService = productsService;
        }

        [HttpGet]
        [Route("Get")]
        public HttpResponseMessage Get()
        {
            return SendResponse(GetProducts(0));
        }

        private MessageWithExceptionResponse GetProducts(int id)
        {
            try
            {
                var response = _productsService.GetProducts(id);
                return new MessageWithExceptionResponse
                {
                    Response = new GenericListResponse<Products>(HttpStatusCode.OK, response)
                };
            }
            catch (Exception ex)
            {
                return new MessageWithExceptionResponse
                {
                    ThrowException = true,
                    Response = new BasicMessageResponse(HttpStatusCode.BadRequest, "Something went wrong!")
                };
            }

        }

        [HttpGet]
        [Route("Get/{productId}")]
        public HttpResponseMessage Get(int productId)
        {
            return SendResponse(GetProducts(productId));
        }

        [HttpGet]
        [Route("Search/{name}")]
        public HttpResponseMessage Search(string name)
        {
            //return SendResponse(GetProductsBySearchTerm(request));
            ProductsSearchRequest request = new ProductsSearchRequest();
            KeyValuePair<string,string> keyval = new KeyValuePair<string, string>("Name", name);
            List<KeyValuePair<string, string>> keyvalList = new List<KeyValuePair<string, string>>();
            keyvalList.Add(keyval);
            request.SearchFields = keyvalList;
            return SendResponse(GetProductsBySearchTerm(request));
        }

        private MessageWithExceptionResponse GetProductsBySearchTerm(ProductsSearchRequest request)
        {

            try
            {
                var response = _productsService.GetProducts(request.SearchFields);
                return new MessageWithExceptionResponse
                {
                    Response = new GenericListResponse<Products>(HttpStatusCode.OK, response)
                };
            }
            catch (Exception ex)
            {
                return new MessageWithExceptionResponse
                {
                    ThrowException = true,
                    Response = new BasicMessageResponse(HttpStatusCode.BadRequest, "Something went wrong!")
                };
            }

        }
    }
}