﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using billingApp.Models;

namespace billingApp.Controllers
{
    public class BaseController:ApiController
    {
        protected HttpResponseMessage SendResponse(MessageWithExceptionResponse response)
        {
            if (response.ThrowException)
            {
                throw new HttpResponseException(response.Response.GetHttpResponseMessage(response.Headers));
            }
            return response.Response.GetHttpResponseMessage(response.Headers);
        }
    }
}