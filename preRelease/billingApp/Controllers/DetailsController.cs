﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using billingApp.Models;
using billingApp.Models.Requests;
using billingApp.Models.Responses;
using billingAppModels;
using billingAppService.Interfaces;

namespace billingApp.Controllers
{
    [RoutePrefix("api/Details")]
    public class DetailsController : BaseController
    {
        private IDetailsService _detailsService;

        public DetailsController(IDetailsService detailsService)
        {
            _detailsService = detailsService;
        }

        [HttpGet]
        [Route("Get")]
        public HttpResponseMessage Get()
        {
            return SendResponse(GetDetails(0));
        }

        private MessageWithExceptionResponse GetDetails(int id)
        {
            try
            {
                var response = _detailsService.GetDetails(id);
                return new MessageWithExceptionResponse
                {
                    Response = new GenericListResponse<Details>(HttpStatusCode.OK, response)
                };
            }
            catch (Exception ex)
            {
                return new MessageWithExceptionResponse
                {
                    ThrowException = true,
                    Response = new BasicMessageResponse(HttpStatusCode.BadRequest, "Something went wrong!")
                };
            }

        }

        [HttpGet]
        [Route("Get/{detailId}")]
        public HttpResponseMessage Get(int detailId)
        {
            return SendResponse(GetDetails(detailId));
        }

        [HttpGet]
        [Route("Search/{billNumber}")]
        public HttpResponseMessage Search(string billNumber)
        {
            DetailsSearchRequest request = new DetailsSearchRequest();
            KeyValuePair<string, string> keyval = new KeyValuePair<string, string>("BillNumber", billNumber);
            List<KeyValuePair<string, string>> keyvalList = new List<KeyValuePair<string, string>>();
            keyvalList.Add(keyval);
            request.SearchFields = keyvalList;
            return SendResponse(GetDetailsBySearchTerm(request));
        }

        private MessageWithExceptionResponse GetDetailsBySearchTerm(DetailsSearchRequest request)
        {

            try
            {
                var response = _detailsService.GetDetails(request.SearchFields);
                return new MessageWithExceptionResponse
                {
                    Response = new GenericListResponse<Details>(HttpStatusCode.OK, response)
                };
            }
            catch (Exception ex)
            {
                return new MessageWithExceptionResponse
                {
                    ThrowException = true,
                    Response = new BasicMessageResponse(HttpStatusCode.BadRequest, "Something went wrong!")
                };
            }

        }
    }
}