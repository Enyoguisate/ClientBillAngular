﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using billingApp.Models;
using billingApp.Models.Requests;
using billingApp.Models.Responses;
using billingAppModels;
using billingAppService.Interfaces;

namespace billingApp.Controllers
{
    [RoutePrefix("api/Clients")]
    public class ClientsController:BaseController
    {
        private IClientsService _clientsService;

        public ClientsController(IClientsService clientsService)
        {
            _clientsService = clientsService;
        }

        [HttpGet]
        [Route("Get")]
        public HttpResponseMessage Get()
        {
            return SendResponse(GetClients(0));
        }

        private MessageWithExceptionResponse GetClients(int id)
        {
            try
            {
                var response = _clientsService.GetClients(id);
                return new MessageWithExceptionResponse
                {
                    Response = new GenericListResponse<Clients>(HttpStatusCode.OK, response)
                };
            }
            catch (Exception ex)
            {
                return new MessageWithExceptionResponse
                {
                    ThrowException = true,
                    Response = new BasicMessageResponse(HttpStatusCode.BadRequest, "Something went wrong!")
                };
            }
            
        }

        [HttpGet]
        [Route("Get/{clientId}")]
        public HttpResponseMessage Get(int clientId)
        {
            return SendResponse(GetClients(clientId));
        }

        [HttpPost]
        [Route("Search")]
        public HttpResponseMessage Search(ClientSearchRequest request)
        {
            return SendResponse(GetClientsBySearchTerm(request));
        }

        private MessageWithExceptionResponse GetClientsBySearchTerm(ClientSearchRequest request)
        {
            
            try
            {
                var response = _clientsService.GetClients(request.SearchFields);
                return new MessageWithExceptionResponse
                {
                    Response = new GenericListResponse<Clients>(HttpStatusCode.OK, response)
                };
            }
            catch (Exception ex)
            {
                return new MessageWithExceptionResponse
                {
                    ThrowException = true,
                    Response = new BasicMessageResponse(HttpStatusCode.BadRequest, "Something went wrong!")
                };
            }

        }
    }
}