﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using billingApp.Models;
using billingApp.Models.Requests;
using billingApp.Models.Responses;
using billingAppModels;
using billingAppService.Interfaces;

namespace billingApp.Controllers
{
    [RoutePrefix("api/Bills")]
    public class BillsController : BaseController
    {
        private IBillsService _billsService;

        public BillsController(IBillsService billsService)
        {
            _billsService = billsService;
        }

        [HttpGet]
        [Route("Get")]
        public HttpResponseMessage Get()
        {
            return SendResponse(GetBills(0));
        }

        private MessageWithExceptionResponse GetBills(int id)
        {
            try
            {
                var response = _billsService.GetBills(id);
                return new MessageWithExceptionResponse
                {
                    Response = new GenericListResponse<Bills>(HttpStatusCode.OK, response)
                };
            }
            catch (Exception ex)
            {
                return new MessageWithExceptionResponse
                {
                    ThrowException = true,
                    Response = new BasicMessageResponse(HttpStatusCode.BadRequest, "Something went wrong!")
                };
            }

        }

        [HttpGet]
        [Route("Get/{billId}")]
        public HttpResponseMessage Get(int billId)
        {
            return SendResponse(GetBills(billId));
        }

        [HttpPost]
        [Route("Search")]
        public HttpResponseMessage Search(BillsSearchRequest request)
        {
            return SendResponse(GetBillsBySearchTerm(request));
        }

        private MessageWithExceptionResponse GetBillsBySearchTerm(BillsSearchRequest request)
        {

            try
            {
                var response = _billsService.GetBills(request.SearchFields);
                return new MessageWithExceptionResponse
                {
                    Response = new GenericListResponse<Bills>(HttpStatusCode.OK, response)
                };
            }
            catch (Exception ex)
            {
                return new MessageWithExceptionResponse
                {
                    ThrowException = true,
                    Response = new BasicMessageResponse(HttpStatusCode.BadRequest, "Something went wrong!")
                };
            }

        }
    }
}