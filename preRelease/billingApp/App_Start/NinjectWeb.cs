﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(billingApp.App_Start.NinjectWeb), "Start")]
namespace billingApp.App_Start
{
    using Microsoft.Web.Infrastructure.DynamicModuleHelper;
    using Ninject.Web.Common;
    public static class NinjectWeb
    {
        public static void Start()
        {
            DynamicModuleUtility.RegisterModule(typeof(NinjectHttpModule));
            
        }
    }
}