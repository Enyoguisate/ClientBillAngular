using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Dependencies;
using billingApp.Controllers;
using billingAppRepository.Interfaces;
using billingAppRepository.Repositories;
using billingAppService.Interfaces;
using billingAppService.Services;
using Ninject.Activation;
using Ninject.Parameters;
using Ninject.Syntax;
using Ninject.Web.WebApi;

[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(billingApp.App_Start.NinjectWebCommon), "Start")]
[assembly: WebActivatorEx.ApplicationShutdownMethodAttribute(typeof(billingApp.App_Start.NinjectWebCommon), "Stop")]

namespace billingApp.App_Start
{
    using System;
    using System.Web;

    using Microsoft.Web.Infrastructure.DynamicModuleHelper;

    using Ninject;
    using Ninject.Web.Common;

    public static class NinjectWebCommon 
    {
        private static readonly Bootstrapper bootstrapper = new Bootstrapper();

        /// <summary>
        /// Starts the application
        /// </summary>
        public static void Start() 
        {
            DynamicModuleUtility.RegisterModule(typeof(OnePerRequestHttpModule));
            DynamicModuleUtility.RegisterModule(typeof(NinjectHttpModule));
            bootstrapper.Initialize(CreateKernel);
        }
        
        /// <summary>
        /// Stops the application.
        /// </summary>
        public static void Stop()
        {
            bootstrapper.ShutDown();
        }
        
        /// <summary>
        /// Creates the kernel that will manage your application.
        /// </summary>
        /// <returns>The created kernel.</returns>
        private static IKernel CreateKernel()
        {
            
            var kernel = new StandardKernel();
            try
            {
                GlobalConfiguration.Configuration.DependencyResolver = new NinjectResolver(kernel);
                kernel.Bind<Func<IKernel>>().ToMethod(ctx => () => new Bootstrapper().Kernel);
                kernel.Bind<IHttpModule>().To<HttpApplicationInitializationHttpModule>();
                RegisterServices(kernel);
                return kernel;
            }
            catch
            {
                kernel.Dispose();
                throw;
            }
        }

        /// <summary>
        /// Load your modules or register your services here!
        /// </summary>
        /// <param name="kernel">The kernel.</param>
        private static void RegisterServices(IKernel kernel)
        {
            kernel.Bind<IClientsService>().To<ClientsService>();
            kernel.Bind<IClientsRepository>().To<ClientsRepository>();
            kernel.Bind<IProductsService>().To<ProductsService>();
            kernel.Bind<IProductsRepository>().To<ProductsRepository>();
            kernel.Bind<IDetailsService>().To<DetailsService>();
            kernel.Bind<IDetailsRepository>().To<DetailsRepository>();
            kernel.Bind<IBillsService>().To<BillsService>();
            kernel.Bind<IBillsRepository>().To<BillsRepository>();
        }
    }

    public class NinjectScope : IDependencyScope
    {
        protected IResolutionRoot iResolutionRoot;
        public NinjectScope(IResolutionRoot kernel)
        {
            iResolutionRoot = kernel;
        }
        
        public void Dispose()
        {
            IDisposable disposable = (IDisposable) iResolutionRoot;
            if (disposable != null)
            {
                disposable.Dispose();
            }
            iResolutionRoot = null;
        }

        public object GetService(Type serviceType)
        {
            IRequest request = iResolutionRoot.CreateRequest(serviceType, null, new Parameter[0], true, true);
            return iResolutionRoot.Resolve(request).SingleOrDefault();
        }

        public IEnumerable<object> GetServices(Type serviceType)
        {
            IRequest request = iResolutionRoot.CreateRequest(serviceType, null, new Parameter[0], true, true);
            return iResolutionRoot.Resolve(request);
        }
    }

    public class NinjectResolver : NinjectScope, IDependencyResolver
    {
        private readonly IKernel kernel;

        public NinjectResolver(IKernel kernel):base(kernel)
        {
            this.kernel = kernel;
        }

        public IDependencyScope BeginScope()
        {
            return new NinjectScope(this.kernel.BeginBlock());
        }
    }
}
