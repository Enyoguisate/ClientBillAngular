angular.module('billingAppModules')
.controller('productsController', [
    '$scope', '$rootScope', 'productsService','productsSearchService', 'listPaginationService', 
    function ($scope, $rootScope, productsService, productsSearchService, listPaginationService) {
        //Set the starting number page
        var currentPage = 1;
        function loadProductsList(){
            productsService.getproducts().$promise.then(function(res){
                //Set original array to the search and pagination service
                productsSearchService.setProductsList(res.list);
                listPaginationService.setItemsList(res.list);
                //Indicates how many pages the array have
                $rootScope.$broadcast('retGetPagTotal',{
                    pageTotal: listPaginationService.getPageTotal()
                });                 
                $scope.productsList = listPaginationService.getItemsPage(currentPage);                        
                
            });
        };

        loadProductsList();

        $rootScope.$on('searchProductBy', function(event,name){
            productsService.search({name:name}).$promise.then(function(res){
                if(res.list.length>0){
                    listPaginationService.setItemsList(res.list);
                    $rootScope.$broadcast('retGetPagTotal',{
                        pageTotal: listPaginationService.getPageTotal()
                    }); 
                    $scope.productsList = res.list;
                }
            });
        });

        $rootScope.$on('noValueOnSearch', function(event){
            loadProductsList();
            
        });
        
        
        //Receive the call to change page
        $rootScope.$on('goToPag',function(event,pag){
            listPaginationService.setPageSelected(pag);
            $scope.productsList = listPaginationService.getItemsPage(pag);
        });

        $rootScope.$on('goToPrevPag',function(event){
            var prevPage = listPaginationService.getPrevPage();
            $scope.productsList = listPaginationService.getItemsPage(prevPage);
        });

        $rootScope.$on('goToNextPag',function(event){
            var nextPage = listPaginationService.getNextPage();
            $scope.productsList = listPaginationService.getItemsPage(nextPage);
        });
        


        
    }]);