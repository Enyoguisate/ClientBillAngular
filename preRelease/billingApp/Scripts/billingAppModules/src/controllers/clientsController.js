﻿angular.module('billingAppModules')
.controller('clientsController', [
    '$scope', '$rootScope', 'clientsService', 'clientsSearchService', 'listPaginationService', 
    function ($scope, $rootScope, clientsService, clientsSearchService, listPaginationService) {
        //Set the starting number page
        var currentPage = 1;
        function loadClientsList(){
            clientsService.getclients().$promise.then(function(res){
                //Set original array to the search and pagination service
                clientsSearchService.setClientsList(res.list);
                listPaginationService.setItemsList(res.list);
               
                //Indicates how many pages the array have
                $rootScope.$broadcast('retGetPagTotal',{
                    pageTotal: listPaginationService.getPageTotal()
                });                 
                $scope.clientsList = listPaginationService.getItemsPage(currentPage);                        
                //$scope.$broadcast('pagData', {clientsList: $scope.clientsList});
            });
        };
        loadClientsList();

        $rootScope.$on('searchClientBy', function (event, params) {
            if (Array.isArray(params)) {
                $scope.clientsList = [];
                clientsService.search({ SearchFields: params }).$promise.then(function(res) {
                    if (res.list.length > 0) {
                        listPaginationService.setItemsList(res.list);
                        $rootScope.$broadcast('retGetPagTotal',{
                            pageTotal: listPaginationService.getPageTotal()
                        }); 
                        $scope.clientsList = res.list;
                    } 
                });
            } 
        });

        $rootScope.$on('noValueOnSearch', function(event){
            loadClientsList();
        });
        
        
        //Receive the call to change page
        $rootScope.$on('goToPag',function(event,pag){
            listPaginationService.setPageSelected(pag);
            $scope.clientsList = listPaginationService.getItemsPage(pag);
        });

        $rootScope.$on('goToPrevPag',function(event){
            var prevPage = listPaginationService.getPrevPage();
            $scope.clientsList = listPaginationService.getItemsPage(prevPage);
        });

        $rootScope.$on('goToNextPag',function(event){
            var nextPage = listPaginationService.getNextPage();
            $scope.clientsList = listPaginationService.getItemsPage(nextPage);
        });
        


        // $rootScope.$on('searchClientBy',function(event, clients){
        //     $scope.clientsList = clients;               
        // });

        
    }]);