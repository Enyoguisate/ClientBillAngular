angular.module('billingAppModules')
.controller('billsController', [
    '$scope', '$rootScope', 'billsService','billsSearchService', 'listPaginationService', 
    function ($scope, $rootScope, billsService, billsSearchService, listPaginationService) {
        //Set the starting number page
        var currentPage = 1;
        function loadBillsList(){
            billsService.getbills().$promise.then(function(res){
                //Set original array to the search and pagination service
                billsSearchService.setBillsList(res.list);
                listPaginationService.setItemsList(res.list);
                //Indicates how many pages the array have
                $rootScope.$broadcast('retGetPagTotal',{
                    pageTotal: listPaginationService.getPageTotal()
                });
                $scope.billsList = listPaginationService.getItemsPage(currentPage);
            });
        };

        loadBillsList();

        $rootScope.$on('searchBillBy', function (event, params) {
            if (Array.isArray(params)) {
                $scope.billsList = [];
                billsService.search({ SearchFields: params }).$promise.then(function(res) {
                    if (res.list.length > 0) {
                        listPaginationService.setItemsList(res.list);
                        $rootScope.$broadcast('retGetPagTotal',{
                            pageTotal: listPaginationService.getPageTotal()
                        }); 
                        $scope.billsList = res.list;
                    } 
                });
            } 
        });

        $rootScope.$on('noValueOnSearch', function(event){
            loadBillsList();
            
        });
        
        
        //Receive the call to change page
        $rootScope.$on('goToPag',function(event,pag){
            listPaginationService.setPageSelected(pag);
            $scope.billsList = listPaginationService.getItemsPage(pag);
        });

        $rootScope.$on('goToPrevPag',function(event){
            var prevPage = listPaginationService.getPrevPage();
            $scope.billsList = listPaginationService.getItemsPage(prevPage);
        });

        $rootScope.$on('goToNextPag',function(event){
            var nextPage = listPaginationService.getNextPage();
            $scope.billsList = listPaginationService.getItemsPage(nextPage);
        });
        


        
    }]);