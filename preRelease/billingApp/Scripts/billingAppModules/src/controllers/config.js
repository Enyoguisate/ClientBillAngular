﻿angular.module('billingAppModules').config(function ($stateProvider, $urlRouterProvider) {
    $stateProvider
    .state('clients', {
        url: '/clients',
        templateUrl: 'Scripts/billingAppModules/src/partials/clients/clients.html',
        controller: 'clientsController'
    })
    .state('products',{
        url: '/products',
        templateUrl: 'Scripts/billingAppModules/src/partials/products/products.html',
        controller: 'productsController'
    })
    .state('details',{
        url: '/details',
        templateUrl: 'Scripts/billingAppModules/src/partials/details/details.html',
        controller: 'detailsController'
    })
    .state('bills',{
        url: '/bills',
        templateUrl: 'Scripts/billingAppModules/src/partials/bills/bills.html',
        controller: 'billsController'
    })
    ;

    $urlRouterProvider.otherwise('/');

    $stateProvider.state('/', {
        url: '/',
        templateUrl: ""
    });

});