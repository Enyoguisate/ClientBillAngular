angular.module('billingAppModules')
.controller('detailsController', [
    '$scope', '$rootScope', 'detailsService','detailsSearchService', 'listPaginationService', 
    function ($scope, $rootScope, detailsService, detailsSearchService, listPaginationService) {
        //Set the starting number page
        var currentPage = 1;
        function loadDetailsList(){
            detailsService.getdetails().$promise.then(function(res){
                //Set original array to the search and pagination service
                detailsSearchService.setDetailsList(res.list);
                listPaginationService.setItemsList(res.list);
                //Indicates how many pages the array have
                $rootScope.$broadcast('retGetPagTotal',{
                    pageTotal: listPaginationService.getPageTotal()
                });                 
                $scope.detailsList = listPaginationService.getItemsPage(currentPage);                        
                
            });
        };

        loadDetailsList();

        $rootScope.$on('searchDetailBy', function(event,billNumber){
            if(billNumber.length>0){
                $scope.detailsList = [];
                detailsService.search({billNumber:billNumber}).$promise.then(function(res){
                    if(res.list.length>0){
                        listPaginationService.setItemsList(res.list);
                        $rootScope.$broadcast('retGetPagTotal',{
                            pageTotal: listPaginationService.getPageTotal()
                        });
                        $scope.detailsList = res.list;
                    }
                });
            }            
        });

        $rootScope.$on('noValueOnSearch', function(event){
            $scope.detailsList = listPaginationService.getItemsPage(currentPage);
        });
        
        
        //Receive the call to change page
        $rootScope.$on('goToPag',function(event,pag){
            listPaginationService.setPageSelected(pag);
            $scope.detailsList = listPaginationService.getItemsPage(pag);
        });

        $rootScope.$on('goToPrevPag',function(event){
            var prevPage = listPaginationService.getPrevPage();
            $scope.detailsList = listPaginationService.getItemsPage(prevPage);
        });

        $rootScope.$on('goToNextPag',function(event){
            var nextPage = listPaginationService.getNextPage();
            $scope.detailsList = listPaginationService.getItemsPage(nextPage);
        });
        


        
    }]);