angular.module('billingAppModules').factory('billsSearchService', function(){
    var billSearchObj = {
        billsArr:[],
        billsOriginalArr:[],
        billsSearchBy: function(inputText){
            if(inputText.lenght>0){
                this.billsArr = [];
                if(billsOriginalArr.indexOf(inputText)!==-1){
                    this.billsArr.push(this);
                }else{
                    this.billsArr.push(this.billsOriginalArr);
                }
            }
        },
        setBillsList: function(billList){
            if(Array.isArray(billList)){
                this.billsArr = billList;
                this.billsOriginalArr = billList;
            }
        },
        getBillsList: function(){
            return this.billsArr;
        }
    };
    return billSearchObj;
});