﻿angular.module('billingAppModules')
    .factory('billSearchDSLFactory',
        function () {
            var info = {
                //na(First Name & Last Name), ph(Phone), em(Email)
                dslList: [
                    { id: 0, label: 'Number',modelName:'Number', dsl: "nu:" },
                    { id: 1, label: 'Total',modelName:'Total', dsl: "to:" }
                ],
                getCategories: function () {
                    return this.dslList;
                },
                textContainsDsl: function (text) {
                    for (var i = 0; i < this.dslList.length; i += 1) {
                        if (text.indexOf(this.dslList[i].dsl) != -1) {
                            return i;
                        }
                    }
                    return -1;
                },
                textWithoutDsl: function (text) {
                    var i = this.textContainsDsl(text);
                    if (i == -1) {
                        return text;
                    } else {
                        return _.replace(text, this.dslList[i].dsl, '');
                    }
                }
            };
            return info;
        });


