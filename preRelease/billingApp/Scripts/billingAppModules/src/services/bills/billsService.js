angular.module('billingAppModules')
.factory('billsService', [
    '$resource', function($resource){
    var billsServiceUrl = 'api/Bills/';
    var billsService = $resource(billsServiceUrl,{},{
        'getbills':{            
            url: billsServiceUrl + 'Get',
            method: 'GET',
            isArray: false
        },
        'search':{
            url: billsServiceUrl + 'Search',
            method: 'POST',
            format: 'JSON',
            data: {
                SearchFields:[]                
            }
        }
    });
    return billsService;
}]);