angular.module('billingAppModules')
.factory('clientsService', [
    '$resource', function($resource){
    var clientsServiceUrl = 'api/Clients/';
    var clientsService = $resource(clientsServiceUrl,{},{
        'getclients':{            
            url: clientsServiceUrl + 'Get',
            method: 'GET',
            isArray: false
        },
        'search':{
            url: clientsServiceUrl + 'Search',
            method: 'POST',
            format: 'JSON',
            data: {
                SearchFields:[]                
            }
        }
    });
    return clientsService;
}]);