angular.module('billingAppModules').factory('clientsSearchService', function(){
    var clientSearchObj = {
        clientsArr:[],
        clientsOriginalArr:[],
        clientsSearchBy: function(inputText){
            if(inputText.lenght>0){
                this.clientsArr = [];
                if(clientsOriginalArr.indexOf(inputText)!==-1){
                    this.clientsArr.push(this);
                }else{
                    this.clientsArr.push(this.clientsOriginalArr);
                }
            }
        },
        setClientsList: function(clientList){
            if(Array.isArray(clientList)){
                this.clientsArr = clientList;
                this.clientsOriginalArr = clientList;
            }
        },
        getClientsList: function(){
            return this.clientsArr;
        }
    };
    return clientSearchObj;
});