angular.module('billingAppModules').factory('listPaginationService', function(){
    var itemsPagObj = {
        pagSelected: 1,
        pagTotal: 1,        
        itemsArr:[],        
        itemsPage:[],
        pagSize: 5,
        getItemsPage: function(pag){
            var max = pag * this.pagSize ;
            var min = max - this.pagSize;
            this.itemsPage = this.itemsArr.slice(min,max);
            return this.itemsPage;
        },
        setItemsList: function(itemList){
            if(Array.isArray(itemList)){
                this.itemsArr = itemList;  
                this.pagTotal = Math.ceil(this.itemsArr.length / this.pagSize);
            }
        },
        getItemsList: function(){
            return this.ItemsArr;
        },
        setPageSelected: function(pageNumber){
            this.pagSelected = pageNumber;
        },
        getPageSelected: function(){
            return this.pagSelected;
        },
        getNextPage: function(){
            if(this.pagSelected < this.pagTotal){
                this.pagSelected++;
            }                
            return this.pagSelected;
        },
        getPrevPage: function(){
            if(this.pagSelected > 1){
                this.pagSelected--;                
            }
            return this.pagSelected;
        },
        getPageTotal: function(){
            return this.pagTotal;
        }
    };
    return itemsPagObj;
});