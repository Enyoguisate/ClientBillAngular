angular.module('billingAppModules')
.factory('detailsService', [
    '$resource', function($resource){
    var detailsServiceUrl = 'api/Details/';
    var detailsService = $resource(detailsServiceUrl,{},{
        'getdetails':{            
            url: detailsServiceUrl + 'Get',
            method: 'GET',
            isArray: false
        },
        'search':{
            url: detailsServiceUrl + 'Search/:billNumber',
            method: 'GET',
            billNumber: '@billNumber'
        }
    });
    return detailsService;
}]);