angular.module('billingAppModules').factory('detailsSearchService', function(){
    var detailSearchObj = {
        detailsArr:[],
        detailsOriginalArr:[],
        detailsSearchBy: function(inputText){
            if(inputText.lenght>0){
                this.clientsArr = [];
                if(detailsOriginalArr.indexOf(inputText)!==-1){
                    this.detailsArr.push(this);
                }else{
                    this.detailsArr.push(this.detailsOriginalArr);
                }
            }
        },
        setDetailsList: function(detailList){
            if(Array.isArray(detailList)){
                this.detailsArr = detailList;
                this.detailsOriginalArr = detailList;
            }
        },
        getDetailsList: function(){
            return this.detailsArr;
        }
    };
    return detailSearchObj;
});