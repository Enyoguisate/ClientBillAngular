angular.module('billingAppModules').factory('productsSearchService', function(){
    var productsearchObj = {
        productsArr:[],
        productsOriginalArr:[],
        productsSearchBy: function(inputText){
            if(inputText.lenght>0){
                this.productsArr = [];
                if(productsOriginalArr.indexOf(inputText)!==-1){
                    this.productsArr.push(this);
                }else{
                    this.productsArr.push(this.productsOriginalArr);
                }
            }
        },
        setProductsList: function(productList){
            if(Array.isArray(productList)){
                this.productsArr = productList;
                this.productsOriginalArr = productList;
            }
        },
        getProductsList: function(){
            return this.productsArr;
        }
    };
    return productsearchObj;
});