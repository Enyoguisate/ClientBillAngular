angular.module('billingAppModules')
.factory('productsService', [
    '$resource', function($resource){
    var productsServiceUrl = 'api/Products/';
    var productsService = $resource(productsServiceUrl,{},{
        'getproducts':{            
            url: productsServiceUrl + 'Get',
            method: 'GET',
            isArray: false
        },
        'search':{
            url: productsServiceUrl + 'Search/:name',
            method: 'GET',
            name:'@name'
            
        }
    });
    return productsService;
}]);