angular.module('billingAppModules')
.directive('listPagination',[
    '$rootScope',function($rootScope){
        return{
            restrict: 'EA',
            replace: true,
            scope:{
                items: '='
            },
            templateUrl:'Scripts/billingAppModules/src/partials/listPagination.html',
            link: function(scope, elem, attr){
                scope.goTo = function(pag){
                    $rootScope.$emit('goToPag',pag);
                };

                scope.goToPrev = function(){
                    $rootScope.$emit('goToPrevPag');
                }

                scope.goToNext = function(){
                    $rootScope.$emit('goToNextPag');
                }                

                $rootScope.$on('retGetPagTotal',function(event,pag){
                    scope.pagTotal = new Array(pag.pageTotal);
                });

            }
        };
    }
]);