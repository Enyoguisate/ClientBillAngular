angular.module('billingAppModules')
.directive('detailsListActions',['$rootScope',
function($rootScope){
    return{
        restrict: 'EA',
        scope: {
            detail: '='
        },
        templateUrl: 'Scripts/billingAppModules/src/partials/details/detailsListActions.html',
        link: function(scope, elem, attr){
            scope.editDetail = function(){
                //$rootScope.$emit('editClient', scope.client);
                alert("editDetail");
            };
            scope.deleteDetail = function(){
                //$rootScope.$emit('deleteClient', scope.client);
                alert("deleteDetail");
            };
        }
    };
}]);