angular.module('billingAppModules')
.directive('detailsSearch',[
    '$rootScope', 'detailsSearchService', function($rootScope,detailsSearchService){
        return{
            restrict: 'EA',
            replace: true,
            scope:true,
            templateUrl:'Scripts/billingAppModules/src/partials/details/detailsSearch.html',
            link: function(scope, elem, attr){                
                scope.searchText = "";
                scope.$watch('searchText',function(newValue,oldValue){                                    
                    if(newValue.length === 0){
                        $rootScope.$emit('noValueOnSearch');
                    }else{
                        $rootScope.$emit('searchDetailBy',newValue);  
                    }
                }) 
            }
        };
    }
]);