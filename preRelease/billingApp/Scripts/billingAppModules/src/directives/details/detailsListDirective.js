angular.module('billingAppModules')
.directive('detailsList',['$rootScope',function($rootScope){
    return{
        restrict: 'EA',
        replace: true,
        scope:{
            details: '='
        },
        templateUrl:'Scripts/billingAppModules/src/partials/details/detailsList.html',
        link: function(scope, elem, attr){

        }
    };
}]);