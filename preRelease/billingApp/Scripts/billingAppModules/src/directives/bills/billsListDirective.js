angular.module('billingAppModules')
.directive('billsList',['$rootScope',function($rootScope){
    return{
        restrict: 'EA',
        replace: true,
        scope:{
            bills: '='
        },
        templateUrl:'Scripts/billingAppModules/src/partials/bills/billsList.html',
        link: function(scope, elem, attr){

        }
    };
}]);