angular.module('billingAppModules')
.directive('billsListActions',['$rootScope',
function($rootScope){
    return{
        restrict: 'EA',
        scope: {
            bill: '='
        },
        templateUrl: 'Scripts/billingAppModules/src/partials/bills/billsListActions.html',
        link: function(scope, elem, attr){
            scope.editBill = function(){
                //$rootScope.$emit('editClient', scope.client);
                alert("editBill");
            };
            scope.deleteBill = function(){
                //$rootScope.$emit('deleteClient', scope.client);
                alert("deleteBill");
            };
        }
    };
}]);