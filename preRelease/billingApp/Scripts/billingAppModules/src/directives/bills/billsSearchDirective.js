angular.module('billingAppModules')
.directive('billsSearch', [
    '$rootScope', 'billsSearchService', 'billSearchDSLFactory', function ($rootScope, billsSearchService, billSearchDSLFactory) {
        return {
            restrict: 'EA',
            replace: true,
            scope: true,
            templateUrl: 'Scripts/billingAppModules/src/partials/bills/billsSearch.html',
            link: function (scope, elem, attr) {
                var paramObj = {
                    key: "string",
                    value: "string"
                };
                scope.searchText = "";
                scope.$watch('searchText', function (newvalue, oldvalue) {
                    var indexDsl = billSearchDSLFactory.textContainsDsl(newvalue);
                    if (indexDsl > -1) {                        
                        var searchParam = [];
                        var opc = billSearchDSLFactory.getCategories()[indexDsl];
                        paramObj.key = opc.modelName;
                        paramObj.value = billSearchDSLFactory.textWithoutDsl(newvalue).trim();
                        searchParam.push(paramObj);
                        // if (opc.id == 0) {
                        //     searchParam.push({ key: "LastName", value: "" });
                        // }
                        if(paramObj.value.length != 0){
                            $rootScope.$emit('searchBillBy', searchParam);
                        }
                        
                    }                 
                    if (scope.searchText.length === 0) {
                        $rootScope.$emit('noValueOnSearch');
                    }
                });
            }
        };
    }
]);