angular.module('billingAppModules')
.directive('productsList',['$rootScope',function($rootScope){
    return{
        restrict: 'EA',
        replace: true,
        scope:{
            products: '='
        },
        templateUrl:'Scripts/billingAppModules/src/partials/products/productsList.html',
        link: function(scope, elem, attr){

        }
    };
}]);