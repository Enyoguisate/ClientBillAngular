angular.module('billingAppModules')
.directive('productsSearch',[
    '$rootScope', function($rootScope){
        return{
            restrict: 'EA',
            replace: true,
            scope:true,
            templateUrl:'Scripts/billingAppModules/src/partials/products/productsSearch.html',
            link: function(scope, elem, attr){                
                scope.searchText = "";
                scope.$watch('searchText',function(newValue,oldValue){                                    
                    if(newValue.length === 0){
                        $rootScope.$emit('noValueOnSearch');
                    }else{
                        $rootScope.$emit('searchProductBy',newValue);  
                    }
                }) 
            }
        };
    }
]);