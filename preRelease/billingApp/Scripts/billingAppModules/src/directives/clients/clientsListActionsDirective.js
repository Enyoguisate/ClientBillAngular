angular.module('billingAppModules')
.directive('clientsListActions',['$rootScope',
function($rootScope){
    return{
        restrict: 'EA',
        scope: {
            client: '='
        },
        templateUrl: 'Scripts/billingAppModules/src/partials/clients/clientsListActions.html',
        link: function(scope, elem, attr){
            scope.editClient = function(){
                //$rootScope.$emit('editClient', scope.client);
                alert("editClient");
            };
            scope.deleteClient = function(){
                //$rootScope.$emit('deleteClient', scope.client);
                alert("deleteClient");
            };
        }
    };
}]);