angular.module('billingAppModules')
.directive('clientsSearch', [
    '$rootScope', 'clientSearchDSLFactory', function ($rootScope, clientSearchDSLFactory) {
        return {
            restrict: 'EA',
            replace: true,
            scope: true,
            templateUrl: 'Scripts/billingAppModules/src/partials/clients/clientsSearch.html',
            link: function (scope, elem, attr) {
                var paramObj = {
                    key: "string",
                    value: "string"
                };
                scope.searchText = "";
                scope.$watch('searchText', function (newvalue, oldvalue) {
                    var indexDsl = clientSearchDSLFactory.textContainsDsl(newvalue);
                    if (indexDsl > -1) {                        
                        var searchParam = [];
                        var opc = clientSearchDSLFactory.getCategories()[indexDsl];
                        paramObj.key = opc.modelName;
                        paramObj.value = clientSearchDSLFactory.textWithoutDsl(newvalue).trim();
                        searchParam.push(paramObj);
                        if (opc.id == 0) {
                            searchParam.push({ key: "LastName", value: "" });
                        }
                        $rootScope.$emit('searchClientBy', searchParam);
                    }                 
                    if (scope.searchText.length === 0) {
                        $rootScope.$emit('noValueOnSearch');
                    }
                });
            }
        };
    }
]);