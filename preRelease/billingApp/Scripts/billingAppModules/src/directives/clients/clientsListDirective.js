angular.module('billingAppModules')
.directive('clientsList',['$rootScope',function($rootScope){
    return{
        restrict: 'EA',
        replace: true,
        scope:{
            clients: '='
        },
        templateUrl:'Scripts/billingAppModules/src/partials/clients/clientsList.html',
        link: function(scope, elem, attr){

        }
    };
}]);