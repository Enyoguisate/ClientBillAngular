﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace billingAppModels
{
    public class Clients
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
        public DateTime Created { get; set; }
        public string CreatedFormated => Created.ToShortDateString();
        public DateTime LastAccess { get; set; }
        public string LastAccessFormated => LastAccess.ToShortDateString();
    }
}
