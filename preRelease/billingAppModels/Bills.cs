﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace billingAppModels
{
    public class Bills
    {
        public int Id { get; set; }
        public DateTime Bdate { get; set; }
        public string BdateFormated => Bdate.ToShortDateString();
        public int Number { get; set; }
        public Decimal Total { get; set; }
        public int IdClient { get; set; }
        //public string ClientName { get; set; }

    }
}
