﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace billingAppModels
{
    public class Details
    {
        public int DetailNumber { get; set; }
        public int BillNumber { get; set; }
        public int IdProduct { get; set; }
        public int Quantity { get; set; }
        public decimal Total { get; set; }
        public string ProductName { get; set; }

    }
}
