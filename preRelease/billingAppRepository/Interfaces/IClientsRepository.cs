﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using billingAppModels;
namespace billingAppRepository.Interfaces
{
    public interface IClientsRepository
    {
        List<Clients> GetClients(int id);
        List<Clients> GetClients();
        List<Clients> GetClients(Clients clientsToSearch);
        List<Clients> GetClients(string searchFieldsList, string searchFieldsValue);
    }
}
