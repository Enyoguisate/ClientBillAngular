﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using billingAppModels;


namespace billingAppRepository.Interfaces
{
    public interface IProductsRepository
    {
        List<billingAppModels.Products> GetProducts(int id);
        List<billingAppModels.Products> GetProducts();
        List<billingAppModels.Products> GetProducts(billingAppModels.Products productsToSearch);
        List<billingAppModels.Products> GetProducts(string searchFieldsList, string searchFieldsValue);
    }
}
