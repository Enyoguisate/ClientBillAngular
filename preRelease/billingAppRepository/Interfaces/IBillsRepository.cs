﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using billingAppModels;

namespace billingAppRepository.Interfaces
{
    public interface IBillsRepository
    {
        List<Bills> GetBills(int id);
        List<Bills> GetBills();
        List<Bills> GetBills(Bills billsToSearch);
        List<Bills> GetBills(string searchFieldsList, string searchFieldsValue);
    }
}
