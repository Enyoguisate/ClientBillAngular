﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using billingAppModels;

namespace billingAppRepository.Interfaces
{
    public interface IDetailsRepository
    {
        List<Details> GetDetails(int id);
        List<Details> GetDetails();
        List<Details> GetDetails(Details detailsToSearch);
        List<Details> GetDetails(string searchFieldsList, string searchFieldsValue);
    }
}
