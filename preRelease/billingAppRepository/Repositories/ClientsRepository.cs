﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using billingAppModels;
using billingAppRepository.Interfaces;

namespace billingAppRepository.Repositories
{
    public class ClientsRepository:Database,IClientsRepository
    {
        public ClientsRepository():base()
        {
            RegisterMapper();
        }

        private void RegisterMapper()
        {
            Mapper.CreateMap<SP_GETCLIENTSBYID_Result, Clients>()
                .ForMember(x=>x.Id,y=>y.MapFrom(z=>z.idClient))
                .ForMember(x => x.FirstName, y => y.MapFrom(z => z.clientFirstName))
                .ForMember(x => x.LastName, y => y.MapFrom(z => z.clientLastName))
                .ForMember(x => x.Address, y => y.MapFrom(z => z.clientAddress))
                .ForMember(x => x.Created, y => y.MapFrom(z => z.clientCreated))
                .ForMember(x => x.Email, y => y.MapFrom(z => z.clientEmail))
                .ForMember(x => x.LastAccess, y => y.MapFrom(z => z.clientLastAccess))
                .ForMember(x => x.Phone, y => y.MapFrom(z => z.clientPhone))
                ;
           Mapper.CreateMap<SP_FIND_STRING_IN_TABLE_CLIENTS_Result, Clients>()
                .ForMember(x => x.Id, y => y.MapFrom(z => z.idClient))
                .ForMember(x => x.FirstName, y => y.MapFrom(z => z.clientFirstName))
                .ForMember(x => x.LastName, y => y.MapFrom(z => z.clientLastName))
                .ForMember(x => x.Address, y => y.MapFrom(z => z.clientAddress))
                .ForMember(x => x.Created, y => y.MapFrom(z => z.clientCreated))
                .ForMember(x => x.Email, y => y.MapFrom(z => z.clientEmail))
                .ForMember(x => x.LastAccess, y => y.MapFrom(z => z.clientLastAccess))
                .ForMember(x => x.Phone, y => y.MapFrom(z => z.clientPhone))
                ;
            Mapper.CreateMap<SP_FIND_STRING_IN_TABLE_CLIENTS_Result, Clients>()
                .ForMember(x => x.Id, y => y.MapFrom(z => z.idClient))
                .ForMember(x => x.FirstName, y => y.MapFrom(z => z.clientFirstName))
                .ForMember(x => x.LastName, y => y.MapFrom(z => z.clientLastName))
                .ForMember(x => x.Address, y => y.MapFrom(z => z.clientAddress))
                .ForMember(x => x.Created, y => y.MapFrom(z => z.clientCreated))
                .ForMember(x => x.Email, y => y.MapFrom(z => z.clientEmail))
                .ForMember(x => x.LastAccess, y => y.MapFrom(z => z.clientLastAccess))
                .ForMember(x => x.Phone, y => y.MapFrom(z => z.clientPhone));
        }

        public List<Clients> GetClients(int id)
        {
            try
            {
                var objDb = InstanceContext.SP_GETCLIENTSBYID(id);
                return Mapper.Map<List<Clients>>(objDb);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Clients> GetClients()
        {
            try
            {
                var objDb = InstanceContext.SP_GETCLIENTSBYID(null);
                return Mapper.Map<List<Clients>>(objDb);
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
        }

        public List<Clients> GetClients(Clients clientsToSearch)
        {
            throw new NotImplementedException();
        }


        public List<Clients> GetClients(string searchFieldsList, string searchFieldsValue)
        {
            //todo create sp that receive two parameters, 
            // one for the fields and another one for the values
            //eg @searchBy = 'ClientFirstName | ClientLastName | ClientEmail' 
            //@searchValues = 'camilo | sexto | casexto@gmail.com'
            try
            {
                //var objDb = InstanceContext.SP_SEARCH_CLIENT_BY_KEY_VALUE(searchFieldsList, searchFieldsList);
                //var objDb = InstanceContext.SP_FIND_STRING_IN_TABLE(searchFieldsList, "dbo", "Clients",searchFieldsValue);
                var objDb = InstanceContext.SP_FIND_STRING_IN_TABLE_CLIENTS(searchFieldsValue, "dbo", "Clients", searchFieldsList);
                return Mapper.Map<List<Clients>>(objDb);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
    }
}
