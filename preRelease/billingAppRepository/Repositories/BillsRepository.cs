﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using billingAppRepository.Interfaces;
using billingAppModels;

namespace billingAppRepository.Repositories
{
    public class BillsRepository:Database,IBillsRepository
    {
        public BillsRepository():base()
        {
            RegisterMapper();
        }

        private void RegisterMapper()
        {
            Mapper.CreateMap<SP_GETBILLSBYID_Result, Bills>()
                .ForMember(x => x.Id, y => y.MapFrom(z => z.idBill))
                .ForMember(x => x.IdClient, y => y.MapFrom(z => z.idClient))
                .ForMember(x => x.Bdate, y => y.MapFrom(z => z.billDate))
                .ForMember(x => x.Number, y => y.MapFrom(z => z.billNumber))
                .ForMember(x => x.Total, y => y.MapFrom(z => z.billTotal));
            Mapper.CreateMap<SP_FIND_STRING_IN_TABLE_BILLS_Result, Bills>()
                .ForMember(x => x.Id, y => y.MapFrom(z => z.idBill ))
                .ForMember(x => x.IdClient, y => y.MapFrom(z => z.idClient))
                .ForMember(x => x.Bdate, y => y.MapFrom(z => z.billDate))
                .ForMember(x => x.Number, y => y.MapFrom(z => z.billNumber))
                .ForMember(x => x.Total, y => y.MapFrom(z => z.billTotal));
        }

        public List<Bills> GetBills(int id)
        {
            try
            {
                var objDb = InstanceContext.SP_GETBILLSBYID(id);
                return Mapper.Map<List<Bills>>(objDb);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Bills> GetBills()
        {
            try
            {
                var objDb = InstanceContext.SP_GETBILLSBYID(null);
                return Mapper.Map<List<Bills>>(objDb);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public List<Bills> GetBills(Bills billsToSearch)
        {
            throw new NotImplementedException();
        }


        public List<Bills> GetBills(string searchFieldsList, string searchFieldsValue)
        {
            try
            {
                var objDb = InstanceContext.SP_FIND_STRING_IN_TABLE_BILLS(searchFieldsValue, "dbo", "Bills", searchFieldsList);
                return Mapper.Map<List<Bills>>(objDb);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
    }
}
