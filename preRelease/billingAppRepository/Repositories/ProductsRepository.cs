﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoMapper;
using System.Threading.Tasks;

using billingAppModels;
using billingAppRepository.Interfaces;

namespace billingAppRepository.Repositories
{
    public class ProductsRepository:Database,IProductsRepository
    {
        public ProductsRepository() : base()
        {
            RegisterMapper();
        }

        private void RegisterMapper()
        {
            Mapper.CreateMap<SP_GETPRODUCTSBYID_Result, Products>()
                .ForMember(x => x.Id, y => y.MapFrom(z => z.idProduct))
                .ForMember(x => x.Name, y => y.MapFrom(z => z.name))
                .ForMember(x => x.Price, y => y.MapFrom(z => z.price))
                .ForMember(x => x.Stock, y => y.MapFrom(z => z.stock));
            Mapper.CreateMap<SP_FIND_STRING_IN_TABLE_PRODUCTS_Result, Products>()
                .ForMember(x => x.Id, y => y.MapFrom(z => z.idProduct))
                .ForMember(x => x.Name, y => y.MapFrom(z => z.name))
                .ForMember(x => x.Price, y => y.MapFrom(z => z.price))
                .ForMember(x => x.Stock, y => y.MapFrom(z => z.stock));
        }
        public List<Products> GetProducts(int id)
        {
            try
            {
                var objDb = InstanceContext.SP_GETPRODUCTSBYID(id);
                return Mapper.Map<List<Products>>(objDb);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Products> GetProducts()
        {
            try
            {
                var objDb = InstanceContext.SP_GETPRODUCTSBYID(null);
                return Mapper.Map<List<Products>>(objDb);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public List<Products> GetProducts(Products productsToSearch)
        {
            throw new NotImplementedException();
        }

        public List<Products> GetProducts(string searchFieldsList, string searchFieldsValue)
        {
            try
            {
                var objDb = InstanceContext.SP_FIND_STRING_IN_TABLE_PRODUCTS(searchFieldsValue, "dbo", "Products", searchFieldsList);
                return Mapper.Map<List<Products>>(objDb);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
