﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using billingAppRepository.Interfaces;
using billingAppModels;

namespace billingAppRepository.Repositories
{
    public class DetailsRepository : Database,IDetailsRepository
    {

        public DetailsRepository():base()
        {
            RegisterMapper();
        }

        private void RegisterMapper()
        {
            Mapper.CreateMap<SP_GETDETAILSBYBILLNUMBER_Result, Details>()
                .ForMember(x => x.DetailNumber, y => y.MapFrom(z => z.detailNumber))
                .ForMember(x => x.BillNumber, y => y.MapFrom(z => z.billNumber))
                .ForMember(x => x.IdProduct, y => y.MapFrom(z => z.idProduct))
                .ForMember(x => x.Quantity, y => y.MapFrom(z => z.quantity))
                .ForMember(x => x.Total, y => y.MapFrom(z => z.TotalDetail))
                .ForMember(x => x.ProductName, y => y.MapFrom(z => z.name))
                ;
            Mapper.CreateMap<SP_FIND_STRING_IN_TABLE_DETAILS_Result, Details>()
                .ForMember(x => x.DetailNumber, y => y.MapFrom(z => z.detailNumber))
                .ForMember(x => x.BillNumber, y => y.MapFrom(z => z.billNumber))
                .ForMember(x => x.IdProduct, y => y.MapFrom(z => z.idProduct))
                .ForMember(x => x.Quantity, y => y.MapFrom(z => z.quantity))
                .ForMember(x => x.Total, y => y.MapFrom(z => z.TotalDetail))
                ;
        }

        public List<Details> GetDetails(int id)
        {
            try
            {
                var objDb = InstanceContext.SP_GETDETAILSBYBILLNUMBER(id);
                return Mapper.Map<List<Details>>(objDb);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Details> GetDetails()
        {
            try
            {
                var objDb = InstanceContext.SP_GETDETAILSBYBILLNUMBER(null);
                return Mapper.Map<List<Details>>(objDb);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public List<Details> GetDetails(Details detailsToSearch)
        {
            throw new NotImplementedException();
        }


        public List<Details> GetDetails(string searchFieldsList, string searchFieldsValue)
        {
            try
            {
                //ahhhhhhhhhhhhhh ver porq explota    
                var objDb = InstanceContext.SP_FIND_STRING_IN_TABLE_DETAILS(searchFieldsValue, "dbo", "Details", searchFieldsList);
                return Mapper.Map<List<Details>>(objDb);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
    }
}
