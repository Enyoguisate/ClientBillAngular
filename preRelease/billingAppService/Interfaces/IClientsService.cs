﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using billingAppModels;

namespace billingAppService.Interfaces
{
    public interface IClientsService
    {
        List<Clients> GetClients(int id);
        List<Clients> GetClients(List<KeyValuePair<string, string>> searchTerms);
        
    }
}
