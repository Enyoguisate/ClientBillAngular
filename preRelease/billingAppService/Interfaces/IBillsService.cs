﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using billingAppModels;
namespace billingAppService.Interfaces
{
    public interface IBillsService
    {
        List<Bills> GetBills(int id);
        List<Bills> GetBills(List<KeyValuePair<string, string>> searchTerms);
    }
}
