﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using billingAppModels;

namespace billingAppService.Interfaces
{
    public interface IProductsService
    {
        List<Products> GetProducts(int id);
        List<Products> GetProducts(List<KeyValuePair<string, string>> searchTerms);
    }
}
