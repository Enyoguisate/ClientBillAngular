﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using billingAppModels;
namespace billingAppService.Interfaces
{
    public interface IDetailsService
    {
        List<Details> GetDetails(int id);
        List<Details> GetDetails(List<KeyValuePair<string, string>> searchTerms);
    }
}
