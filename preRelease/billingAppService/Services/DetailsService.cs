﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using billingAppModels;
using billingAppRepository.Interfaces;
using billingAppService.Interfaces;

namespace billingAppService.Services
{
    public class DetailsService : IDetailsService
    {
        private IDetailsRepository _detailsRepository;

        public DetailsService(IDetailsRepository detailsRepository)
        {
            this._detailsRepository = detailsRepository;
        }

        public List<Details> GetDetails(int id)
        {
            return id == 0 ? _detailsRepository.GetDetails() : _detailsRepository.GetDetails(id);
        }

        public List<Details> GetDetails(List<KeyValuePair<string, string>> searchTerms)
        {
            List<string> searchByStr = new List<string>();
            List<string> searchValuesStr = new List<string>();
            foreach (var item in searchTerms)
            {
                foreach (var prop in typeof(Details).GetProperties())
                {
                    if (prop.Name == item.Key)
                    {
                        searchByStr.Add(item.Key);
                        searchValuesStr.Add(item.Value);
                    }
                }
            }
            var searchFieldsDb = String.Join("|", searchByStr);
            var searchValuesDb = searchValuesStr[0];
            return _detailsRepository.GetDetails(searchFieldsDb, searchValuesDb);
        }
    }
}
