﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using billingAppModels;
using billingAppRepository.Interfaces;
using billingAppService.Interfaces;

namespace billingAppService.Services
{
    public class ProductsService:IProductsService
    {
        private IProductsRepository _productsRepository;

        public ProductsService(IProductsRepository productsRepository)
        {
            this._productsRepository = productsRepository;
        }

        public List<Products> GetProducts(int id)
        {
            return id == 0 ? _productsRepository.GetProducts() : _productsRepository.GetProducts(id);
        }

        public List<Products> GetProducts(List<KeyValuePair<string, string>> searchTerms)
        {
            List<string> searchByStr = new List<string>();
            List<string> searchValuesStr = new List<string>();
            foreach (var item in searchTerms)
            {
                foreach (var prop in typeof(Products).GetProperties())
                {
                    if (prop.Name == item.Key)
                    {
                        searchByStr.Add(item.Key);
                        searchValuesStr.Add(item.Value);
                    }
                }
            }
            var searchFieldsDb = String.Join("|", searchByStr);
            var searchValuesDb = String.Join("|", searchValuesStr);
            return _productsRepository.GetProducts(searchFieldsDb, searchValuesDb);
        }
    }
}
