﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using billingAppModels;
using billingAppRepository.Interfaces;
using billingAppService.Interfaces;

namespace billingAppService.Services
{
    public class BillsService : IBillsService
    {
        
            private IBillsRepository _billsRepository;

            public BillsService(IBillsRepository billsRepository)
            {
                this._billsRepository = billsRepository;
            }

            public List<Bills> GetBills(int id)
            {
                return id == 0 ? _billsRepository.GetBills() : _billsRepository.GetBills(id);
            }

            public List<Bills> GetBills(List<KeyValuePair<string, string>> searchTerms)
            {
                List<string> searchByStr = new List<string>();
                List<string> searchValuesStr = new List<string>();
                foreach (var item in searchTerms)
                {
                    foreach (var prop in typeof (Bills).GetProperties())
                    {
                        if (prop.Name == item.Key)
                        {
                            searchByStr.Add(item.Key);
                            searchValuesStr.Add(item.Value);
                        }
                    }
                }
                var searchFieldsDb = String.Join("|", searchByStr);
                var searchValuesDb = searchValuesStr[0];
                return _billsRepository.GetBills(searchFieldsDb, searchValuesDb);
            }
    }
}