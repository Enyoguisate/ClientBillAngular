﻿using System;
using System.Collections.Generic;
using System.Linq;
using billingAppModels;
using billingAppRepository.Interfaces;

using billingAppService.Interfaces;

namespace billingAppService.Services
{
    public class ClientsService : IClientsService
    {
        private IClientsRepository _clientsRepository;
        
        public ClientsService(IClientsRepository clientsRepository)
        {
            this._clientsRepository = clientsRepository;
        }

        public List<Clients> GetClients(int id)
        {
            return id == 0 ? _clientsRepository.GetClients() : _clientsRepository.GetClients(id);
        }

        public List<Clients> GetClients(List<KeyValuePair<string, string>> searchTerms)
        {
            List<string> searchByStr = new List<string>();
            List<string> searchValuesStr = new List<string>();
            foreach (var item in searchTerms)
            {
                foreach (var prop in typeof(Clients).GetProperties())
                {
                    if (prop.Name == item.Key)
                    {
                        searchByStr.Add(item.Key);
                        searchValuesStr.Add(item.Value);
                    }
                }
            }
            var searchFieldsDb = String.Join("|", searchByStr);
            var searchValuesDb =searchValuesStr[0];
            return _clientsRepository.GetClients(searchFieldsDb, searchValuesDb);
        }
    }
}
