﻿using System.Web;
using System.Web.Optimization;

namespace testClienteFactura
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Content/bootstrap/js/bootstrap.js",
                      "~/Scripts/login.js",
                      "~/Scripts/respond.js"));
            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/font-awesome.min.css",
                      "~/Content/ionicons.min.css",
                      "~/Content/site.css",
                      "~/Content/bootstrap - 3.3.7.min.css",
                      "~/Content/jquery-ui.css",
                      "~/Content/jquery.dataTables.min.css"
                      ));
            bundles.Add(new ScriptBundle("~/myScripts/customScripts").Include(
                "~/Scripts/myScripts/jquery-3.2.1.min.js",
                "~/Scripts/myScripts/bootstrap-3.3.7.min.js",
                "~/Scripts/myScripts/typeahead.bundle.js",
                "~/Scripts/myScripts/jquery-ui.js",
                "~/Scripts/myScripts/jquery.maskedinput.js",
                "~/Scripts/myScripts/TablesHandler.js",
                "~/Scripts/myScripts/ModalNewBill.js"
                ));

        }
    }
}
