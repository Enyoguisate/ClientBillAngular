﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using testClientBillBLL;
using Bills = testClientBillModels.Bills;


namespace testClienteFactura.Controllers
{
    public class HomeController : Controller
    {
        private testClientBillBLL.Clients _ClientsBll;
        private testClientBillBLL.Bills _BillsBll;
        private testClientBillBLL.Details _DetailsBll;
        private testClientBillBLL.Products _ProductBll;

        public HomeController():base()
        {
            _ClientsBll = new Clients();
            _BillsBll = new testClientBillBLL.Bills();
            _DetailsBll = new Details();
            _ProductBll = new Products();

        }
        public ActionResult Index()
        {
            return View(_ClientsBll.GetClientList(0));
        }

        [HttpGet]
        public JsonResult GetBillById(int billId)
        {
            List<Bills> bills = _BillsBll.GetBillsListByClientId(billId);
            return Json(new {Results = bills}, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetDetailsByBillNumber(int billNumber)
        {
            List<testClientBillModels.Detail> details = _DetailsBll.GetDetailsList(billNumber);
            return Json(new { Results = details }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetProductsByIdProduct(int idProduct)
        {
            List<testClientBillModels.Products> products = _ProductBll.GetProductsById(idProduct);
            return Json(new { Results = products }, JsonRequestBehavior.AllowGet);
        }
        

    }
}