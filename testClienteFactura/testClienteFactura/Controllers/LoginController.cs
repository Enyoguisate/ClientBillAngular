﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using testClientBillModels;
using testClientBillBLL;
using Users = testClientBillModels.Users;

namespace testClienteFactura.Controllers
{
    public class LoginController : Controller
    {
        private testClientBillBLL.Login _LoginBll = new testClientBillBLL.Login();

        // GET: Login
        [HttpGet]
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(Users user)
        {
            if (ModelState.IsValid)
            {
                if (_LoginBll.ValidateUserCredentials(user.UserUsername, user.UserPassword))
                {
                    FormsAuthentication.SetAuthCookie(user.UserUsername, user.RememberMe);
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    ModelState.AddModelError("", "Login data is incorrect!");
                }

            }
            return View(user);
        }

        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Login", "Login");
        }
    }
}