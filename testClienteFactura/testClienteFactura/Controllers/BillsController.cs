﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using testClientBillModels;
using BillsBll = testClientBillBLL.Bills;
using ClientsBll = testClientBillBLL.Clients;
using DetailsBll = testClientBillBLL.Details;
using ProductsBll = testClientBillBLL.Products;

namespace testClienteFactura.Controllers
{
    public class BillsController : Controller
    {
        private BillsBll _BillBll = new BillsBll();
        private DetailsBll _DetailBll = new DetailsBll();
        private ClientsBll _clientsBll = new ClientsBll();
        private ProductsBll _productsBll = new ProductsBll();


        //********************************************
        //              Show Bills List
        //********************************************
        [HttpGet]
        public ActionResult List()
        {

            return View(_BillBll.GetBillsList(0));
        }

        [HttpGet]
        public ActionResult New()
        {
            return View();
        }
        //********************************************
        //          Show Bills Add Form
        //********************************************
        [HttpGet]
        public ActionResult Add(Bills bill)
        {

            return View(bill);
        }

        //********************************************
        //              Add Bill 
        //********************************************
        [HttpPost]
        public ActionResult AddBill(Bills bill)
        {
            _BillBll.RegistarBill(bill);
            return View("List", _BillBll.GetBillsList(0));
        }

        //********************************************
        //              Show Bill Edit
        //********************************************
        [HttpGet]
        public ActionResult Edit(Bills bill)
        {
            foreach (var item in _BillBll.GetBillsList(bill.IdBill))
            {
                bill.ClientName = item.ClientName;
            }

            return View(bill);
        }

        //********************************************
        //              Edit Bill
        //********************************************
        [HttpPost]
        public ActionResult EditBill(Bills bill)
        {
            bill.BillDate = DateTime.Today;
            _BillBll.ChangeBill(bill);
            return View("List", _BillBll.GetBillsList(0));
        }

        //********************************************
        //              Delete Bill
        //********************************************
        [HttpGet]
        public ActionResult Delete(int idBill)
        {
            if (idBill != 0) _BillBll.RemoveBill(idBill);
            return View("List", _BillBll.GetBillsList(0));
        }

        //********************************************
        //         Show Clients Bills List
        //********************************************
        [HttpGet]
        public ActionResult ClientBill(Bills bill)
        {
            return View(_BillBll.GetClientBills(bill.IdClient, bill.IdBill));
        }

        [HttpGet]
        public ActionResult DeleteByClientId(int idClient)
        {
            if (idClient != 0)
            {
                List<Bills> billsList = _BillBll.GetBillsListByClientId(idClient);
                if (billsList != null)
                    foreach (var item in billsList)
                    {
                        List<Detail> DetailsList = _DetailBll.GetDetailsList(item.BillNumber);
                        if (DetailsList != null)
                            foreach (var item2 in DetailsList)
                            {
                                _DetailBll.DeteleDetailByBillNumber(item.BillNumber);
                            }
                        _BillBll.RemoveBill(item.BillNumber);
                    }
                _clientsBll.RemoveClient(idClient);
            }
            return RedirectToAction("List", "Clients");
        }

        [HttpGet]
        public JsonResult GetBillsByClientId(int clientId)
        {
            List<Bills> bills = _BillBll.GetBillsListByClientId(clientId);
            return Json(new { Results = bills }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetBillsByBillNumber(int billNumber)
        {
            List<Detail> details = _DetailBll.GetDetailsList(billNumber);
            return Json(new { Results = details }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult DeleteBillByBillNumber(int billNumber)
        {
            if (billNumber != 0)
            {
                List<Detail> detailsList = _DetailBll.GetDetailsList(billNumber);
                if (detailsList != null)
                    foreach (var item in detailsList)
                    {
                        _DetailBll.DeteleDetailByBillNumber(item.BillNumber);
                    }
                List<Bills> billsList = _BillBll.GetBillsList(0);
                Bills bill = new Bills();
                if (billsList != null)
                    foreach (var item2 in billsList)
                    {
                        if (billNumber == item2.BillNumber)
                            bill = item2;
                    }
                _BillBll.RemoveBill(bill.IdBill);
            }
            return RedirectToAction("List", "Bills");
        }

        [HttpGet]
        public JsonResult GetProducts(int page)
        {
            List<Products> productsList = _productsBll.GetProductsById(0);
            List<Products> productsListToShow = new List<Products>();
            if (page == 1)
            {
                productsListToShow = productsList.GetRange(0, 11);
            }else if (page == productsList.Count/10)
            {
                productsListToShow = productsList.GetRange(productsList.Count - 11, 11);
            }
            else
            {
                productsListToShow = productsList.GetRange(productsList.Count / 10 * page, 11);
            }
            return Json(new { Results = productsListToShow }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult AddSelectedProduct(int productId)
        {
            List<Products> productsSelected = _productsBll.GetProductsById(productId);
            return Json(new { Results = productsSelected }, JsonRequestBehavior.AllowGet);
        }

       
        [HttpPost]
        public JsonResult CreateNewDetail(string[] items)
        {
            List<Products> productsList = new List<Products>();
            List<Detail> newDetailList = new List<Detail>();
            if (items != null)
            {
                for (int i = 0; i < items.Length; i += 4)
                {
                    Products products = new Products();
                    products.IdProduct = Int32.Parse(items[i]);
                    products.Name = items[i + 1];
                    products.Price = Decimal.Parse(items[i + 2]);
                    products.Stock = Int32.Parse(items[i + 3]);
                    productsList.Add(products);
                }
                foreach (var item in productsList)
                {
                    Detail newDetail = new Detail();
                    newDetail.IdProduct = item.IdProduct;
                    newDetail.ProductName = item.Name;
                    newDetail.Quantity = 1;
                    newDetail.TotalDetail = item.Price * newDetail.Quantity;
                    newDetailList.Add(newDetail);
                }
            }
            return Json(new { Results = newDetailList }, JsonRequestBehavior.AllowGet);
        }

        public class ClientModel 
        {
            public int IdClient { get; set; }
            public string ClientName { get; set; }
        }

        [HttpGet]
        public JsonResult GetClients(string partial)
        {
            List<ClientModel> clientListMock = new List<ClientModel>();
            if (partial != null)
            {
                List<Clients> clientsList = _clientsBll.GetClientList(0);

                foreach (var client in clientsList)
                {
                    var clientFullName = client.ClientFirstName + " " + client.ClientLastName;
                    if (clientFullName.Contains(partial.ToUpper()))
                    {
                        ClientModel clientMock = new ClientModel();
                        clientMock.IdClient = client.IdClient;
                        clientMock.ClientName = client.ClientFirstName + " " + client.ClientLastName;
                        clientListMock.Add(clientMock);
                    }
                }
            }
            return Json(new { Results = clientListMock }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GetClientData(int idClient)
        {
            Clients client = new Clients();

            return Json(new { Results = "asd" }, JsonRequestBehavior.AllowGet);
        }

        public class ClientFormat
        {
            public string ClientName { get; set; }
            public int IdClient { get; set; }
            public string ClientAddress { get; set; }
            public string ClientEmail { get; set; }
        }

        [HttpGet]
        public JsonResult GetClient(int optionSelId)
        {
            List<testClientBillModels.Clients> clientsList = _clientsBll.GetClientList(optionSelId);
            List<ClientFormat> clientFormatList = new List<ClientFormat>();
            foreach (var client in clientsList)
            {
                ClientFormat clientMock = new ClientFormat();
                clientMock.ClientName = client.ClientFirstName + " " + client.ClientLastName;
                clientMock.IdClient = client.IdClient;
                clientMock.ClientAddress = client.ClientAddress;
                clientMock.ClientEmail = client.ClientEmail;
                clientFormatList.Add(clientMock);
            }
            return Json(new {Results = clientFormatList }, JsonRequestBehavior.AllowGet);
        }


        /*for (int i = 0; i < items.Length; i += 4)
                {
                    Products products = new Products();
                    products.IdProduct = Int32.Parse(items[i]);
                    products.Name = items[i + 1];
                    products.Price = Decimal.Parse(items[i + 2]);
                    products.Stock = Int32.Parse(items[i + 3]);
                    productsList.Add(products);
                }*/
        [HttpPost]
        public JsonResult SaveDetail(int billNumber, string[] items)
        {
            if (items != null)
            {
                Detail newDetail = new Detail();
                for (int i = 0; i < items.Length; i+=4)
                {
                    newDetail.BillNumber = billNumber;
                    newDetail.IdProduct = Int32.Parse(items[i]);
                    newDetail.Quantity = Int32.Parse(items[i + 2]);
                    newDetail.TotalDetail = Decimal.Parse(items[i+3]);
                    _DetailBll.AddDetail(newDetail);
                }
            }
            return Json(new { Results = true }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SaveBill(int clientId, string[] items)
        {
            var billNumber = _BillBll.GetLastBillNumber();
            if (items != null)
            {
                var billTotal = 0;
                for (int i = 0; i < items.Length; i+=4)
                {
                    billTotal += Int32.Parse(items[i + 3]);
                }
                Bills newBill = new Bills();
                newBill.BillNumber = billNumber;
                newBill.IdClient = clientId;
                newBill.BillDate = DateTime.Now;
                newBill.BillTotal = billTotal;
                _BillBll.RegistarBill(newBill);
            }
            return Json(new {Results = billNumber }, JsonRequestBehavior.AllowGet);
        }


    }
}