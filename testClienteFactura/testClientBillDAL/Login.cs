﻿using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using testClientBillModels;

namespace testClientBillDAL
{
    public class Login : Database
    {
        public Login() : base()
        {
        }

        public bool ValidateUsername(string username, string password)
        {
            using (var cmd = GetCommand("[SP_VALUSEREXISTENCE]", CommandType.StoredProcedure))
            {
                cmd.Parameters.Add("@userUsername", SqlDbType.VarChar).Value = username;
                cmd.Parameters.Add("@userPassword", SqlDbType.VarChar).Value = password;
                using (var reader = cmd.ExecuteReader())
                {
                    return reader.Read();
                }
            }
        }

    }
}
