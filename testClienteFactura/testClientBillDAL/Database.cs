﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Web.Configuration;

namespace testClientBillDAL
{
    public class Database
    {
        public const string CONNECTION_NAME = "myDbConection";
        public static string _connectionString = String.Empty;
        protected SqlConnection Connection { get; private set; }
        public static string ConnectionString
        {
            get
            {
                if (_connectionString == String.Empty)
                {
                    _connectionString = WebConfigurationManager.ConnectionStrings[CONNECTION_NAME].ConnectionString;
                }
                return _connectionString;
            }
        }
        public Database()
        {
            if (this.Connection == null)
            {
                this.Connection = new SqlConnection(ConnectionString);
            }
        }
        public SqlCommand GetCommand(string commandText, CommandType commandType)
        {
            OpenConnection();
            return new SqlCommand(commandText, this.Connection)
            {
                CommandType = commandType
            };
        }
        public void CloseConnection()
        {
            if (this.Connection.State != ConnectionState.Closed)
                this.Connection.Close();
        }
        private void OpenConnection()
        {
            if (this.Connection.State != ConnectionState.Open)
                this.Connection.Open();
        }
        
    }
}
