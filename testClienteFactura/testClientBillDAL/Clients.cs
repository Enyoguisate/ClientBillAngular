﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using testClientBillModels;

namespace testClientBillDAL
{
    public class Clients : Database
    {
        
        public List<testClientBillModels.Clients> GetClientsById(int id)
        {
            List<testClientBillModels.Clients> clientsList = new List<testClientBillModels.Clients>();
            try
            {
                using (var cmd = GetCommand("[SP_GETCLIENTSBYID]", CommandType.StoredProcedure))
                {
                    if (id != 0)cmd.Parameters.AddWithValue("@idClient", id);
                    using (var reader = cmd.ExecuteReader())
                    {
                        if (!reader.HasRows) return null;
                        int idxClientId = reader.GetOrdinal("idClient");
                        int idxClientFirstName = reader.GetOrdinal("clientFirstName");
                        int idxClientLastName = reader.GetOrdinal("clientLastName");
                        int idxClientEmail = reader.GetOrdinal("clientEmail");
                        int idxClientPhone = reader.GetOrdinal("clientPhone");
                        int idxClientAddress = reader.GetOrdinal("clientAddress");
                        int idxClientCreated = reader.GetOrdinal("clientCreated");
                        int idxClientLastAccess = reader.GetOrdinal("clientLastAccess");

                        while (reader.Read())
                        {
                            testClientBillModels.Clients client = new testClientBillModels.Clients();
                            client.IdClient = reader.GetInt32((idxClientId));
                            client.ClientFirstName = reader.GetString((idxClientFirstName));
                            client.ClientLastName = reader.GetString((idxClientLastName));
                            client.ClientEmail = reader.GetString((idxClientEmail));
                            client.ClientPhone = reader.GetString((idxClientPhone));
                            client.ClientAddress = reader.GetString((idxClientAddress));
                            client.ClientCreated = reader.GetDateTime(idxClientCreated);
                            client.ClientLastAccess = reader.GetDateTime(idxClientLastAccess);

                            clientsList.Add(client);
                        }
                        return clientsList;
                    }
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                CloseConnection();
            }
        }

        public bool InsertClient(testClientBillModels.Clients client)
        {
            bool aState = false;
            try
            {
                using (var cmd = GetCommand("[SP_INSCLIENTS]", CommandType.StoredProcedure))
                {
                    cmd.Parameters.AddWithValue("@clientFirstName", client.ClientFirstName);
                    cmd.Parameters.AddWithValue("@clientLastName", client.ClientLastName);
                    cmd.Parameters.AddWithValue("@clientEmail", client.ClientEmail);
                    cmd.Parameters.AddWithValue("@clientPhone", client.ClientPhone);
                    cmd.Parameters.AddWithValue("@clientAddress", client.ClientAddress);
                    cmd.Parameters.AddWithValue("@clientCreated", client.ClientCreated);
                    cmd.Parameters.AddWithValue("@clientLastAccess", client.ClientLastAccess);
                    cmd.ExecuteNonQuery();
                    if (cmd.ExecuteNonQuery() != 0) aState = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                CloseConnection();
            }
            return aState;
        }

        public bool UpdateClient(testClientBillModels.Clients client)
        {
            try
            {
                var reader = 0;
                using (var cmd = GetCommand("[SP_UPDCLIENT]", CommandType.StoredProcedure))
                {
                    cmd.Parameters.AddWithValue("@idClient", client.IdClient);
                    cmd.Parameters.AddWithValue("@clientFirstName", client.ClientFirstName);
                    cmd.Parameters.AddWithValue("@clientLastName", client.ClientLastName);
                    cmd.Parameters.AddWithValue("@clientEmail", client.ClientEmail);
                    cmd.Parameters.AddWithValue("@clientPhone", client.ClientPhone);
                    cmd.Parameters.AddWithValue("@clientAddress", client.ClientAddress);
                    reader = cmd.ExecuteNonQuery();
                }
                return (reader != 0);
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                CloseConnection();
            }
        }


        public bool DeleteClientById(int id)
        {
            var aState = false;
            try
            {
                using (var cmd = GetCommand("[SP_DELCLIENTBYID]", CommandType.StoredProcedure))
                {
                    cmd.Parameters.AddWithValue("@idClient", id);
                    var reader = cmd.ExecuteNonQuery();
                    if (reader < 0)
                    {
                        aState = true;
                    }
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                CloseConnection();
            }
            return aState;
        }









    }
}
