﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace testClientBillDAL
{
    public class Users:Database
    {
        public Users() : base() { }

        public bool InsertUser(testClientBillModels.Users user)
        {
            bool aState = false;
            try
            {
                using (var cmd = GetCommand("[SP_INSUSER]", CommandType.StoredProcedure))
                {
                    cmd.Parameters.AddWithValue("@userUsername", user.UserUsername);
                    cmd.Parameters.AddWithValue("@userPassword", user.UserPassword);
                    cmd.ExecuteNonQuery();
                    aState = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                CloseConnection();
            }
            return aState;
        }

        
    }
}

