﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Runtime.CompilerServices;

namespace testClientBillModels
{
    public class Clients
    {
        [DisplayName("Client ID")]
        public int IdClient { get; set; }

        [DisplayName("First name")]
        public string ClientFirstName { get; set; }

        [DisplayName("Last name")]
        public string ClientLastName { get; set; }

        [Required(ErrorMessage = "Email is Required")]
        [RegularExpression(@"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}" +
                           @"\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\" +
                           @".)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$",
                           ErrorMessage = "Email is not valid")]
        [DisplayName("Email")]
        [DataType(DataType.EmailAddress)]
        public string ClientEmail { get; set; }

        [DisplayName("Phone")]
        public string ClientPhone { get; set; }

        [DisplayName("Address")]
        public string ClientAddress { get; set; }

        [DisplayName("Created")]
        public DateTime ClientCreated { get; set; }
        public string ClientCreatedFormated => ClientCreated.ToShortDateString();

        [DisplayName("Last access")]
        public DateTime ClientLastAccess { get; set; }
        public string ClientLastAccessFormated => ClientLastAccess.ToShortDateString();

        


    }
}
