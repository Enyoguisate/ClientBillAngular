﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace testClientBillModels
{
    public class Bills
    {
        [Display(Name = "Id Bill")]
        public int IdBill { get; set; }
        [Display(Name = "Bill Date")]
        public DateTime BillDate { get; set; }
        public string BillDateFormated => BillDate.ToShortDateString();
        [Display(Name = "Bill Number")]
        public int BillNumber { get; set; }
        [Display(Name = "Bill Total")]
        public Decimal BillTotal { get; set; }
        [Display(Name = "Id Client")]
        public int IdClient { get; set; }
        public string ClientName { get; set; }

    }
}
