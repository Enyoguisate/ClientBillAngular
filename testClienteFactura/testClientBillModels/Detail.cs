﻿namespace testClientBillModels
{
    public class Detail
    {
        public int DetailNumber { get; set; }
        public int BillNumber { get; set; }
        public int IdProduct { get; set; }
        public int Quantity { get; set; }
        public decimal TotalDetail { get; set; }
        public string ProductName { get; set; }


    }
}