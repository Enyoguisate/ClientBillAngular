﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Helpers;
using System.Web.Http;
using System.Web.Http.Results;
using System.Web.Mvc;
using testClientBillBLL;
using Clients = testClientBillModels.Clients;

namespace ClientsBillAPI.Controllers
{
    
    [System.Web.Http.RoutePrefix("Clients")]
    public class ClientsController : ApiController
    {
        private testClientBillBLL.Clients _ClientBll;

        public ClientsController ()
        {
            _ClientBll = new testClientBillBLL.Clients();
        }

        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("GetClients")]
        public JsonResult<List<Clients>> GetClients()
        {
            _ClientBll = new testClientBillBLL.Clients();
            return Json(_ClientBll.GetClientList(0));
        }

        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("UpdateClient")]
        public Boolean UpdateClient(Clients client)
        {
            if (client != null)
            {
                return _ClientBll.ChangeClient(client);

            }
            return false;
        }

        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("DeleteClient/{clientId}")]
        public Boolean DeleteClient(int clientId)
        {
            if (clientId != 0)
            {
                //return _ClientBll.RemoveClient(client.IdClient);
                return true;
            }
            return false;
        }

        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("CreateClient")]
        public Boolean CreateClient(Clients client)
        {
            if (client != null)
            {
                //return _ClientBll.createClient(client.IdClient);
                return true;
            }
            return false;
        }
        
    }
}