﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Helpers;
using System.Web.Http;
using System.Web.Http.Results;
using testClientBillModels;
using Bills = testClientBillModels.Bills;

namespace ClientsBillAPI.Controllers
{
    [RoutePrefix("Bills")]
    public class BillsController : ApiController
    {
        private testClientBillBLL.Bills _BillsBll;

        public BillsController()
        {
            testClientBillBLL.Bills _BillsBll = new testClientBillBLL.Bills();
        }

        [HttpGet]
        [Route("GetBills")]
        public JsonResult<List<Bills>> GetBills()
        {
            _BillsBll = new testClientBillBLL.Bills();
            return Json( _BillsBll.GetBillsList(0));
        }

        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("UpdateBill")]
        public Boolean UpdateBill(testClientBillModels.Bills bill)
        {
            if (bill != null)
            {
                //return _BillsBll.ChangeBill(bill);
                return true;
            }
            return false;
        }

        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("DeleteBill/{billId}")]
        public Boolean DeleteBill(int billId)
        {
            if (billId != 0)
            {
                //return _BillsBll.RemoveBill(billId);
                return true;
            }
            return false;
        }

        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("CreateBill")]
        public Boolean CreateBill(testClientBillModels.Bills bill)
        {
            if (bill != null)
            {
                //return _BillsBll.RegistarBill(bill);
                return true;
            }
            return false;
        }
    }
}