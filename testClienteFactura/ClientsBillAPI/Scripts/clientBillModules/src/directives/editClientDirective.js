angular.module('clientBillModule')
.directive('editClient',[
    '$rootScope',
    '$uibModal', 
    function($rootScope,$uibModal){
        return{
            restrict: 'EA',
            replace: true,
            scope: true,
            link: function(scope, elem, attr) {
                $rootScope.$on('editClient', function(event,client){
                    scope.clientSelected = angular.copy(client);
                    var modalInstance = $uibModal.open({
                    templateUrl: 'Scripts/clientBillModules/src/partials/editClient.html',
                    controller: 'modalController',
                    scope: scope
                    });
                    modalInstance.result.then(function(client){
                        $rootScope.clientListReload = true;
                    });
                });
            }
        }
}]);

