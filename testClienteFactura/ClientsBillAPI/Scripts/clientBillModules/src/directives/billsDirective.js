angular.module('clientBillModule')
.directive('billsList',[
    'billsSearch','$rootScope', function(billsSearch,$rootScope) {
    return {
        restrict: 'EA',
        replace: true,
        scope: true,
        templateUrl: 'Scripts/clientBillModules/src/partials/billsList.html',
        link: function(scope, elem, attr) {
            scope.$watch(
                function(){
                return billsSearch.getBillsList();
                },
                function(){
                    scope.bills = billsSearch.getBillsList();
                }    
            )
            scope.bills = billsSearch.getBillsList();
            scope.searchText = "";
            scope.$watch('searchText',function(n,o){                
                billsSearch.billsSearchByText(scope.searchText);                
            });

            scope.newBill = function(){
                $rootScope.$emit('newBill');
            };
            
            
        }
    };
}]);

