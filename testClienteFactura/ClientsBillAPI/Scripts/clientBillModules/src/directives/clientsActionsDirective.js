angular.module('clientBillModule')
.directive('clientActions',['$rootScope',
function($rootScope) {
    return {
        restrict: 'EA',
        scope: {
            client:'='
        },
        templateUrl: 'Scripts/clientBillModules/src/partials/clientActions.html',
        link: function (scope, elem, attr) {
            scope.editClient = function () {
                $rootScope.$emit('editClient', scope.client);                
            };  
            scope.deleteClient = function() {
                $rootScope.$emit('deleteClient', scope.client);                
            };     
        }
    };
}]);