﻿angular.module('clientBillModule').directive('clientSelected', function() {
    return {
        restrict: 'EA',
        replace: true,
        scope: {
            clients: '='
        },
        templateUrl: 'Scripts/clientBillModules/src/partials/clientSelected.html',
        link: function(scope, elem, attr) {
           
        }
    }
});

