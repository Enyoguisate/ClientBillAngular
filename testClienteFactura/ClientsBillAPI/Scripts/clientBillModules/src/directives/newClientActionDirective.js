angular.module('clientBillModule')
.directive('newClientaction',['$rootScope',function($rootScope) {
    return {
        restrict: 'EA',
        scope: true,
        templateUrl: 'Scripts/clientBillModules/src/partials/newClientAction.html',
        link: function (scope, elem, attr) {
            scope.newClient = function () {
                $rootScope.$emit('newClient', scope);                
            };              
        }
    };
}]);