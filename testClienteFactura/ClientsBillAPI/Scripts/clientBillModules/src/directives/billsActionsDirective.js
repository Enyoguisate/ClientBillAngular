angular.module('clientBillModule')
.directive('billActions',['$rootScope',
function($rootScope){
    return{
        restrict: 'EA',
        scope: {
            bill: '='
        },
        templateUrl: 'Scripts/clientBillModules/src/partials/billsActions.html',
        link: function(scope, elem, attr){
            scope.editBill = function(){
                $rootScope.$emit('editBill', scope.bill);
            };
            scope.deleteBill = function(){
                $rootScope.$emit('deleteBill', scope.bill);
            };
        }
    };
}]);