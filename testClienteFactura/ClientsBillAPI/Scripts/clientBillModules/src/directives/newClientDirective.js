angular.module('clientBillModule')
.directive('newClient',[
    '$rootScope',
    '$uibModal',
    function($rootScope,$uibModal) {
        return {
            restrict: 'EA',
            replace: true,
            scope: true,        
            link: function (scope, elem, attr) {
                $rootScope.$on('newClient',function(event, scope){
                    var modalInstance = $uibModal.open({
                        templateUrl: 'Scripts/clientBillModules/src/partials/newClient.html',
                        controller: 'modalController',
                        scope: scope
                    });
                    modalInstance.result.then(function(){
                        
                    });
                });            
            }
        };
}]);
