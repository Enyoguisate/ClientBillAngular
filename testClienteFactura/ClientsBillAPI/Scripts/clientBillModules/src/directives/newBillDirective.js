angular.module('clientBillModule')
.directive('newBill',[
    '$rootScope',
    '$uibModal',
    function($rootScope,$uibModal) {
        return {
            restrict: 'EA',
            replace: true,
            scope: true,        
            link: function (scope, elem, attr) {                
                $rootScope.$on('showNewBillModal',function(event, scope){
                    var modalInstance = $uibModal.open({
                        templateUrl: 'Scripts/clientBillModules/src/partials/newBill.html',
                        controller: 'modalController',
                        scope: scope
                    });
                    modalInstance.result.then(function(){
                        
                    });
                });            
            }
        };
}]);
