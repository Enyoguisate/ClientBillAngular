angular.module('clientBillModule').directive('clientsList', function() {
    return {
        restrict: 'EA',
        replace: true,
        scope: {
            clients: '='
        },
        templateUrl: 'Scripts/clientBillModules/src/partials/clientsList.html',
        link: function(scope, elem, attr) {
           
        }
    }
});

