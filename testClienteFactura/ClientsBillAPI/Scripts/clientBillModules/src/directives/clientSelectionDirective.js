﻿angular.module('clientBillModule')
.directive('clientSelection',[
    '$rootScope','clientsService','clientsSearch', 
    function($rootScope,clientsService,clientsSearch) {
    return {
        restrict: 'EA',
        replace: true,
        scope: {
            clients: '='
        },
        templateUrl: 'Scripts/clientBillModules/src/partials/clientSelection.html',
        link: function(scope, elem, attr) {
            clientsService.getclients().$promise.then(function(res){
                clientsSearch.setClientsList(res);
                scope.clients = clientsSearch.getClientsList();                
            });           
        }
    }
}]);

