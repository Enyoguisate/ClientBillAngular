angular.module('clientBillModule')
.directive('editBill',[
    '$rootScope',
    '$uibModal', 
    function($rootScope,$uibModal){
        return{
            restrict: 'EA',
            replace: true,
            scope: true,
            link: function(scope, elem, attr) {
                $rootScope.$on('editBill', function(event,bill){
                    scope.billSelected = angular.copy(bill);
                    var modalInstance = $uibModal.open({
                    templateUrl: 'Scripts/clientBillModules/src/partials/editBill.html',
                    controller: 'modalController',
                    scope: scope
                    });                    
                });
            }
        };
}]);
