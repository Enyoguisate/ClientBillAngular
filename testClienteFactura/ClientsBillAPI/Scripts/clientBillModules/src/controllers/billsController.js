angular.module('clientBillModule').controller('billsController', [
'$scope', 'billsService','billsSearch', '$rootScope', function ($scope, billsService, billsSearch,$rootScope) {
    $scope.showSearch = false;
    $scope.bills = [];

    function loadBillList(){
        billsService.getbills().$promise.then(function(res){
            var billsList = res;
            billsSearch.setBillsList(billsList);         
        });
    };

    loadBillList();

    $rootScope.$on('editBill', function(event, bill){
        $scope.billSelected = bill;
    });

    $scope.$broadcast('billSelected', {
        billSelected: $scope.billSelected
    });

    $rootScope.$on('deleteBill', function(event, bill){
        swal({
            title: "Are you sure to delete this bill?",
            text:  bill.ClientName,
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "mmm... ok!",
            closeOnConfirm: false
        },
        function () {
            if(bill != null){                
                billsService.deletebill({billId:bill.IdBill}).$promise.then(function(res){
                    if(!res.$resolved){
                        alert("something went wrong!");
                        loadBillList();
                    }else{                        
                        swal("bill deleted", "success");
                        loadBillList();
                    }
                });
            }                 
        });        
    });

    $rootScope.$on('reloadBillList',function(event){
        loadBillList();
    });

    $rootScope.$on('submitBillEdited', function(event, bill){
        $scope.billEdited = bill;
    });

    $scope.$watch(function () {
        return $scope.billSelected;
    });

    $rootScope.$on('newBill',function(event){
        $rootScope.$broadcast('showNewBillModal');         
    });

    
}]);