angular.module('clientBillModule').controller('modalController', [
'$scope','$uibModalInstance','clientsService','billsService', '$rootScope',
    function ($scope, $uibModalInstance, clientsService, billsService, $rootScope) {
        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
        
        $scope.submitClientForm = function(client){
            $scope.clientEdited = angular.copy(client);
            clientsService.updateclient($scope.clientEdited).$promise.then(function(res){
                if(res == false){
                    alert("something went wrong!");
                }else{
                    $rootScope.$emit('reloadList');
                }                     
            });
            $uibModalInstance.close(true); 
        };

        $scope.createClient = function(client){
            $scope.newClient = angular.copy(client);
            clientsService.createclient($scope.newClient).$promise.then(function(res){
                if(res == false){
                    alert("something went wrong!");                    
                }else{
                    $rootScope.$emit('reloadList');
                }
            });
            $uibModalInstance.close(true);
        };

        $scope.submitBillForm = function(bill){
            $scope.billEdited = angular.copy(bill);
            billsService.updatebill($scope.billEdited).$promise.then(function(res){
                if(res == false){
                    alert("something went wrong!");
                }else{
                    $rootScope.$emit('reloadBillList');
                }    
            });
            $uibModalInstance.close(true); 
        };
        
        $scope.submitNewBillForm = function(newBill){

        };
}]);
