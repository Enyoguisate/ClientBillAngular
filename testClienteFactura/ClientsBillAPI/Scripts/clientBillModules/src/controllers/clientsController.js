angular.module('clientBillModule').controller('clientsController', [
    '$scope', 'clientsService', '$rootScope', function ($scope, clientsService, $rootScope) {
        function loadList() {
            clientsService.getclients().$promise.then(function (res) {
                $scope.clientsList = res;
            });
        };

        loadList();

        $rootScope.$on('editClient', function (event, client) {
            $scope.clientSelected = client;
        });

        $scope.$broadcast('clientSelected', {
            clientSelected: $scope.clientSelected
        });

        $rootScope.$on('submitClientEdited', function (event, client) {
            $scope.clientEdited = client;            
        });

        $scope.$watch(function () {
            return $scope.clientSelected;
        });

        $rootScope.$on('deleteClient', function(event, client){
            swal({
                title: "Are you sure to delete this client?",
                text: client.ClientFirstName + " " + client.ClientLastName,
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "mmm... ok!",
                closeOnConfirm: false
            },
            function () {
                if(client != null){                    
                    clientsService.deleteclient({clientId:client.IdClient}).$promise.then(function(res){
                        if(!res.$resolved){
                            alert("something went wrong!");
                            loadList();
                        }else{
                            swal("client deleted", "success");
                            loadList();
                        }
                    });
                }                   
            });            
        });

        $rootScope.$on('newClient',function(event, scope){
            $rootScope.$broadcast('showNewClientModal', {scope});         
        });

        $rootScope.$on('reloadList', function (event) {
            loadList();
        });        

        

    }]);