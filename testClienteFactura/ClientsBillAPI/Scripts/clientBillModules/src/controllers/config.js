angular.module('clientBillModule').config(function ($stateProvider, $urlRouterProvider) {    
    $stateProvider.state('clients', {
        url: '/clients',
        templateUrl: 'Scripts/clientBillModules/src/partials/clients.html',
        controller: 'clientsController'
    });
    
    $stateProvider.state('bills',{
        url:'/bills',
        templateUrl: 'Scripts/clientBillModules/src/partials/bills.html',
        controller: 'billsController'
    });
    
    $urlRouterProvider.otherwise('/');

    $stateProvider.state('/',{
      url:'/',
      templateUrl: ""
    })
    
});