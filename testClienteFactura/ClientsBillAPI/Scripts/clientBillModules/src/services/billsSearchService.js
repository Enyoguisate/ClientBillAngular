angular.module('clientBillModule').factory('billsSearch', function(){
    var searchObj={
        billsArr:[],
        billOriginalArr:[],
        billsSearchByText: function(inputText){
            if(inputText.length>0){
                this.billsArr = [];
                this.billsArr.push(this.billOriginalArr[0]);
            }else{
                this.billsArr = this.billOriginalArr
            }
        },
        getBillsList: function(){
            return this.billsArr;
        },
        setBillsList: function(aList){
            if(Array.isArray(aList)){
                this.billsArr = aList;
                this.billOriginalArr = aList;
            }
            
        }
    };
    return searchObj;
});