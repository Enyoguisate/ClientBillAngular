angular.module('clientBillModule').factory('billsService', [
    '$resource', function($resourse){
        var billsServiceUrl = '/Bills/';
        var billsService = $resourse(billsServiceUrl,{},{
            'getbills':{
                url: billsServiceUrl + 'GetBills',
                method: 'GET',
                isArray: true
            },
            'updatebill':{
                url: billsServiceUrl + 'UpdateBill',
                method: 'POST',
                data: ':bill'
            },
            'deletebill': {
                url: billsServiceUrl + 'DeleteBill/:billId',
                method: 'GET',
                params:{billId:'@billId'}
            },
            'createBill':{
                url: billsServiceUrl + 'CreateBill',
                method: 'POST',
                data: ':bill'
            }
        });
        return billsService;
    }
]);