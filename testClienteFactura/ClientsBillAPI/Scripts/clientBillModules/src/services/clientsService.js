angular.module('clientBillModule')
.factory('clientsService', [
    '$resource', function($resource) {
    var clientsServiceUrl = '/Clients/';
    var clientsService = $resource(clientsServiceUrl, {}, {
        'getclients': {
            url: clientsServiceUrl + 'GetClients',
            method: 'GET',
            isArray: true
        },
        'updateclient': {
            url: clientsServiceUrl + 'UpdateClient',
            method: 'POST',
            data: ':client'        
        },
        'deleteclient': {
            url: clientsServiceUrl + 'DeleteClient/:clientId',
            method: 'GET',
            params:{clientId: '@clientId'}
        },
        'createclient': {
            url: clientsServiceUrl + 'CreateClient',
            method: 'POST',
            data: ':client'        
        }        
        });
    return clientsService;
    }
]);