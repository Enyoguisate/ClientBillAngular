﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using testClientBillDAL;

namespace testClientBillBLL
{
    public class Details
    {
        private testClientBillDAL.Details _DetailsDal;

        public Details()
        {
            _DetailsDal = new testClientBillDAL.Details();
        }

        public List<testClientBillModels.Detail> GetDetailsList(int billNumber)
        {
            return _DetailsDal.GetDetailsById(billNumber);
        }

        public bool DeteleDetailByBillNumber(int billNumber)
        {
            return _DetailsDal.DeleteDetailByBillNumber(billNumber);
        }

        public bool AddDetail(testClientBillModels.Detail detail)
        {
            return _DetailsDal.InsertDetail(detail);
        }

        
    }
}
