﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using testClientBillModels;
using testClientBillDAL;

namespace testClientBillBLL
{
    public class Login
    {
        private testClientBillDAL.Login _loginDal;

        public Login()
        {
            _loginDal = new testClientBillDAL.Login();
        }

        public bool ValidateUserCredentials(string username, string password)
        {

            return _loginDal.ValidateUsername(username, password);

        }


    }
}
